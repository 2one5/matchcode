<?php get_header(); ?>
<?php   global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(627, WPGRADE_PREFIX.'page_html_title', true);?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail_by_postid(627,'full', 'featured-image', true );?>
			</div>
        <?php $hide_title = get_post_meta(627, WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <div class="wrapper entry-header-wrapper dgimg">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title exp_title"><?php echo $title=get_the_title(627);?></h1>
                        <h4 class="exp_top_h"><?php echo $meta_values = get_post_meta(627,'_senna_page_html_title',true ); ?></h4>
                    </header><!-- .entry-header -->
                </div>
            <?php }?>
        	<div class="wrapper entry-header-wrapper blue_tab h46">&nbsp;</div>
		<?php if (have_posts()) : while (have_posts()) : the_post();?>    
			<div class="wrapper">
                <div class="container ourstr">
                	<div class="tagnames">
                		<?php 
							$tags=wp_get_post_tags( get_the_ID());
							for($i=0;$i<count($tags);$i++)
							{
								echo $tags[$i]->name;
								if(($i+1)<(count($tags))) echo ' , ';
							}
						?>
                	</div>
                    <hr />
					<h1 class="psttole">
						<?php 
							$title=get_the_title(); 
							$title=explode(" ",$title);
							for($i=0;$i<count($title);$i++)
							{
								if($i==0)
								{
									echo '<b>'.$title[$i].' </b>';
								}
								else
								{
									echo ' '.$title[$i];
								}
							}
						?>
                    </h1>
                </div>
            </div>
            <div class="wrapper dgimg"> 
            	<div class="container">
                	<div class="careers" id="str__dliser"> 
					<?php $flg=get_all_fetured_imgfor_check(get_the_ID());
					if($flg){?> 
            		<div id="storyslider">
                    	<ul class="slides">
                        <?php 
						if( class_exists( 'kdMultipleFeaturedImages' ) ) 
						{
							echo get_all_fetured_imgfor_post(get_the_ID());
						}?>		
                        </ul>
                    </div>
                    <?php } ?>
                    <div class="slidercnt">    
                    	<?php the_content() ?>
                    </div>
                    </div>
                </div>
            </div> 
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
            
<?php endwhile; endif;?>
<?php get_footer(); ?>
<script>
jQuery(document).ready(function(){
	jQuery("#menu-item-738 a").css("background","#fff");
	jQuery("#menu-item-738 a").css("color","#004990");
	jQuery("#menu-item-752 a").css("Font-Weight","bold");
	jQuery("#menu-item-752 a").css("color","#fff");
});	
</script>
 <script type="text/javascript">
    jQuery( ".slides li" ).each(function() {
	  var tag=jQuery(this).html();
	  if(tag=='')
	  {
		  jQuery(this).remove();
	  }
	  else
	  {
		  //jQuery("#storyslider").append('<li>'+tag+'</li>');
	  }
	});
	jQuery('.slides').bxSlider();
  </script>
