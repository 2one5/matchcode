<?php get_header(); ?>
    <?php
		global $wpGrade_Options;
		
		if (have_posts()) : while (have_posts()) : the_post();
		if ( $wpGrade_Options->get('portfolio_single_show_header_image') ) {
		$html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'project_html_title', true);
		if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
			<div class="wrapper page-header-wrapper">
				<?php wpgrade_get_thumbnail('full', 'featured-image', true );
				$html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'project_html_title', true);
				if ( !empty($html_title) ) {?>
					<div class="container">
						<div class="row">
							<section class="page-header span12 bigger-headings">
								<?php wpgrade_display_content( $html_title ); ?>
							</section>
						</div>
					</div>
				<?php } ?>
			</div>
		<?php }
		} ?>
        <div class="wrapper page-wrapper" itemscope itemtype="http://schema.org/WebPage">
            <div class="container">
                <div class="row row-single-project">
                    <div class="span7">
                       <?php portfolio_single_gallery(get_the_ID()) ?>
                    </div>
                    <div class="span5 l_portfolio_content fade_in">
                        <div class="navigation-portfolio">
                            <?php wpgrade_content_nav('single-nav'); ?>
                        </div>
						<?php
						$hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'project_display_title', true);
						//we hide the title when the author requested it but only if the header image is active
						if ( $hide_title != "on" || !$wpGrade_Options->get('portfolio_single_show_header_image')) {  ?>
                        <h1 itemprop="name"><?php the_title(); ?></h1>
                        <?php
						}
						the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

    <?php //comments_template();
    endwhile; endif; ?>
<?php get_footer(); ?>