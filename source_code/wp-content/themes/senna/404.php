<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package wpGrade
 * @since wpGrade 1.0
 */
?>

<?php get_header(); ?>
<div id="content">
  	<div class="wrapper">
        <div class="container">
            <h1 class="heading-404">404</h1>
            
			<article id="post-0" class="post error404 not-found">
				<h2 class="entry-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'wpgrade' ); ?></h2>
				
				<p><?php _e( 'This may be because of a mistyped URL, faulty referral or out-of-date search engine listing. <br />
							   You should try the <a href="'.home_url().'">homepage</a> instead or maybe do a search?', 'wpgrade' ); ?></p>
				
				<div class="search-form">
					<?php get_search_form(); ?>
				</div> 	
			</article>
        </div>
    </div>
</div> <!-- end #content -->

<?php get_footer(); ?>