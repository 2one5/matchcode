<?php 
/*
Template Name: Experience Tab 
*/
get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);

        if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail('full', 'featured-image', true );

                if ( !empty($html_title) ) {?>
                    <!--<div class="container">
                        <div class="row">
                            <section class="page-header span12 bigger-headings">
                                <?php wpgrade_display_content( $html_title ); ?>
                            </section>
                        </div>
                    </div>-->
                <?php }?>
            </div>
        <?php } ?>
        <div class="wrapper entry-header-wrapper dgimg mpzero">
        	<?php include (TEMPLATEPATH.'/blue_bdr.php'); ?>
        </div>    
		<?php 
        $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <div class="wrapper entry-header-wrapper dgimg mpzero">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title exp_title">
							<?php  
								$title=get_the_title();
								$tittle=explode(" ",$title);	
								for($t=0;$t<count($tittle);$t++)
								{
									if($t==0)
									{ 
										if(get_the_ID()==485)
										{
											echo '<span class="exp_t_one_brg">'.$tittle[$t].'</span>';
										}
										else if(get_the_ID()==494)
										{
											echo '<span class="exp_t_one">'.$tittle[$t].'</span>';
										}
										else if(get_the_ID()==579 || get_the_ID()==577)
										{
											echo '<span class="exp_t_one_one">'.$tittle[$t].'</span>';
										}
										else
										{
											echo '<span class="exp_t_one_sc">'.$tittle[$t].'</span>';
										}
									}
									else
									{ 
										if(get_the_ID()==485)
										{
											echo ' <span class="exp_t_two_two_brg">'.$tittle[$t].'</span>';											
										}
										else if(get_the_ID()==579 || get_the_ID()==577)
										{
											echo ' <span class="exp_t_two_two">'.$tittle[$t].'</span>';											
										}
										else 
										{
											echo ' <span class="exp_t_two">'.$tittle[$t].'</span>';}
										}
									}
							?></h1>
                        <h4 class="exp_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4>
                    </header><!-- .entry-header -->
                </div>
            <?php }?>
			
            <div class="wrapper">
                <div class="container">
                	<div class="ourteam">
                  		<?php the_content() ?>
                	</div>
                </div>
            </div>
            <div class="wrapper">
               <!--  <div class="black_bg">CHECK OUT OUR EXPERIENCE SAMPLES, AND CONTACT US TODAY TO GET YOUR BUSINESS CONNECTED!</div> -->
			<!-- Thumbnail Gallery -->
				<!-- <div class="exp_gallery">
                        <?php //get_all_fetured_img(485) ?>
                </div> -->

            </div> 
            <div class="wrapper mpzero">
            	<div class="clint_buzz">
                	<span class="hrline"><hr /></span>
                    <h2><?php 
					/* Testimonials
					array(27) { 
						["ID"]=> int(451) 
						["post_author"]=> string(1) "1" 
						["post_date"]=> string(19) "2013-06-10 11:59:33" 
						["post_date_gmt"]=> string(19) "2013-06-10 11:59:33" 
						["post_content"]=> string(210) "Matchcode proved to be the right agency to work with, as their "precision profiling" service helped us to identify potential business partners in new industries and new regions, in a very fast and targeted way." 
						["post_title"]=> string(17) "Channel Marketing" 
						["post_excerpt"]=> string(0) "" 
						["post_status"]=> string(7) "publish" 
						["comment_status"]=> string(6) "closed" 
						["ping_status"]=> string(6) "closed" 
						["post_password"]=> string(0) "" 
						["post_name"]=> string(13) "testimonial-1" 
						["to_ping"]=> string(0) "" 
						["pinged"]=> string(0) "" 
						["post_modified"]=> string(19) "2013-08-16 19:45:03" 
						["post_modified_gmt"]=> string(19) "2013-08-16 19:45:03" 
						["post_content_filtered"]=> string(0) "" 
						["post_parent"]=> int(0) 
						["guid"]=> string(67) "http://pixelgrade.com/demos/senna/?post_type=testimonial&p=451" 
						["menu_order"]=> int(1) 
						["post_type"]=> string(11) "testimonial" 
						["post_mime_type"]=> string(0) "" 
						["comment_count"]=> string(1) "0" 
						["filter"]=> string(3) "raw" 
						["ancestors"]=> array(0) { } 
						["post_category"]=> array(0) { } 
						["tags_input"]=> array(0) { } 
					}
					array(4) { 
						["_edit_last"]=> array(1) { [0]=> string(1) "1" } 
						["_edit_lock"]=> array(1) { [0]=> string(12) "1376687075:1" } 
						["_senna_author_name"]=> array(1) { [0]=> string(13) "Tobias Hacker" } 
						["_senna_author_function"]=> array(1) { [0]=> string(9) "Secure IT" } 
					} 
					*/

						$clientbuzz=get_option('client_buzz');
						$clientbuzz=explode(" ",$clientbuzz);
						for($i=0;$i<count($clientbuzz);$i++)
						{ 
						// Styling for 'Client Buzz'	
							if($i==0)
							{
								if(get_the_ID()==485)
								{
									echo '<span class="exp_t_one_brg">'.$clientbuzz[$i].'</span>';
								}
								else if(get_the_ID()==490)
								{
									echo '<span class="ctb_one_sc">'.$clientbuzz[$i].'</span>';
								}
								elseif(get_the_ID()==579 || get_the_ID()==577)
								{
									echo '<span class="exp_t_one_one">'.$clientbuzz[$i].'</span>';
								}
								else
								{
									echo '<span class="ctb_one">'.$clientbuzz[$i].'</span>';
								}
							}
							else
							{
								if(get_the_ID()==485)
								{
									echo ' <span class="exp_t_two_two_brg">'.$clientbuzz[$i].'</span>';
								}
								else if(get_the_ID()==490)
								{
									echo ' <span class="ctb_two_sc">'.$clientbuzz[$i].'</span>';
								}
								elseif(get_the_ID()==579 || get_the_ID()==577)
								{
									echo ' <span class="exp_t_two_two">'.$clientbuzz[$i].'</span>';
								}
								else
								{
									echo ' <span class="ctb_two">'.$clientbuzz[$i].'</span>';
								}
							}
							switch (get_the_ID()) {
								case 485:
									$testID = 945;
									break;
								case 490:
									$testID = 946;
									break;
								case 579:
									$testID = 452;
									break;
								case 577:
									$testID = 451;
									break;
								default:
									# code...
									break;
							}
							$testimonial = get_post($testID, ARRAY_A);
							$testimonial_content = $testimonial['post_content'];
							$author = get_post_custom($testID);
							$author_name = $author['_senna_author_name'][0];
							$author_function = $author['_senna_author_function'][0];
						}

					?></h2>
                    <p><?php echo '<span><span class="ctb_one_sc">"</span> '.$testimonial_content.' <span class="qtclose">"</span></span> ';?></p>
<!--                     <div class="client_signature">
                    	<?php // $sname=get_option( 'client_buzz_signature' ); if(!empty($sname))echo '<img src="'.$upload_dir['baseurl'].'/member_image/'.get_option( 'client_buzz_signature' ).'" height="100" width="200" />'; ?>
                    </div> -->
                    <div class="clientname">
                    	<?php echo $author_name.', '.$author_function; ?> 
                    </div>
                </div>
            </div>
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
            
<?php endwhile; endif;
get_footer(); ?>
<script>
jQuery(document).ready(function(){
	menuhover(626,751);
});	
</script>