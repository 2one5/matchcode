<?php 
/*
Template Name: Home Page
*/
get_header();
global $wpGrade_Options;
global $wpdb;
$upload_dir = wp_upload_dir();
    if ( $wpGrade_Options->get('homepage_use_slider') ) {

        $hps_query = new WP_Query(array(
            'post_type' => 'homepage_slide',
            'posts_per_page' => '-1',
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ));
        if ( $hps_query->have_posts() ) : ?>

            <div class="slider slider-front-page loading" width="500px">
                <div class="slider-pattern"></div>
                <div class="flexslider wp_slider" id="frontpage_slider">
                    <ul class="slides">
                        <?php while ( $hps_query->have_posts() ) : $hps_query->the_post(); ?>
                            <li class="slide">

                                <?php $image = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'homepage_slide_image', true);
                                if ( !empty($image) ) {
                                    echo '<div class="featured-image" style="background-image:url(\''.$image.'\')"></div>';
                                }
								
                                $slide_has_video = false;
                                $the_video = '';
								
								//let's see what kind of slide we have
								//leave it commented for now since there are some that have video slides without the post format set
								//if (get_post_format() == 'video') {
									//get the ratio
									$video_width = absint(get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_width', true));
									if (empty($video_width) || $video_width == 0) {
										$video_width = 500;
									}
									$video_height = absint(get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_height', true));
									if (empty($video_height) || $video_height == 0) {
										$video_height = 281;
									}
									$video_ratio = $video_width / $video_height;
									
									//let's see with what kind of video are we dealing
									$videos = wpgrade_post_videos_ids(get_the_ID());

									isset( $videos['youtube'] ) ?  $youtube_id = $videos['youtube'] : $youtube_id = '';
									isset( $videos['vimeo'] ) ? $vimeo_id = $videos['vimeo'] : $vimeo_id = '';

									if ( !empty($youtube_id) ) {
										$the_video = '<div class="youtube_frame" id="ytplayer_'.get_the_ID().'" data-ytid="'.$youtube_id.'"></div>';
										$slide_has_video = true;
									} elseif ( !empty($vimeo_id) ) {
										$the_video = '<iframe class="vimeo_frame" width="500" height="'.intval(500 / $video_ratio).'" id="video_'.get_the_ID().'" src="http://player.vimeo.com/video/'.$vimeo_id.'?api=1&player_id=video_'.get_the_ID().'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
										$slide_has_video = true;
										} elseif( !empty( $video_embed ) ) {
											$slide_has_video = true;
											$the_video = '<div class="video-wrap">' . stripslashes(htmlspecialchars_decode($video_embed)) . '</div>';
											} else {
												$video_m4v = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_m4v', true);
												$video_webm = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_webm', true);
												$video_ogv = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_ogv', true);
												$video_poster = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'video_poster', true);

												if ( !empty($video_m4v) || !empty($video_webm) || !empty($video_ogv) || !empty($video_poster) ) {
													$slide_has_video = true;
													ob_start();
													wpGrade_video_selfhosted(get_the_ID());
													$the_video = ob_get_clean();
												}
											}
								//}
								?>
                                <div class="container">
                                    <div class="row">
                                        <div class="slide-content <?php if ( $slide_has_video ) echo 's-video'?>">

                                            <?php
                                            if ( $slide_has_video ) {
                                                echo '<div class="slide-video" >'.$the_video.'</div>';
                                            }
                                            $caption = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'homepage_slide_caption', true);
											echo '<div';
                                            if ( $slide_has_video ) echo ' class="slide-content-wrapper" ';
											echo '>';
                                            if ( !empty($caption) ) {
                                                echo do_shortcode($caption);
                                            }
                                            $label = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'homepage_slide_label', true);
                                            $link = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'homepage_slide_link', true);
                                            if ( !empty($label) && !empty($link) ) {
                                                echo '<a href="'.$link.'" class="btn btn-slider btn-transparent">'.$label.'</a>';
                                            }
											echo '</div>';
											?>

                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        <?php endif;
    } // end if use slider

    if ( $wpGrade_Options->get('homepage_content1') ) {?>

	<div id="fttwt" class="wrapper w100">
		<div class="container">
			<?php 	//Commented by ideavte 17 july 2013 
					//echo do_shortcode($wpGrade_Options->get('homepage_content1')); 
					if ( is_active_sidebar( 'news-twitter' ) ) : 
						dynamic_sidebar( 'news-twitter' ); 
					endif;
			?>
		</div>
    </div>
    <?php }
    if ( $wpGrade_Options->get('homepage_use_portfolio') ) {?>
    <?php echo $wpGrade_Options->get('homepage_portfolio_fullwidth') ? '' : '<div class="container">' ?>
	<div class="wrapper" id="frontapagetitle">
		<?php
			if ( $wpGrade_Options->get('homepage_portfolio_template') == 'patchwork' ) {
				get_front_page_pachwork_by_category();
			} elseif ( $wpGrade_Options->get('homepage_portfolio_template') == 'single' ) {
				portfolio_front_page_single();
			} elseif ( $wpGrade_Options->get('homepage_portfolio_template') == 'bigsmall' ) {
				portfolio_front_page_bigsmall();
			}
		?>
	</div>
	<?php echo $wpGrade_Options->get('homepage_portfolio_fullwidth') ? '' : '</div>' ?>
    <?php }
    if ( $wpGrade_Options->get('homepage_content2') ) {?>

        <div class="wrapper mpzero">
        	<?php echo do_shortcode($wpGrade_Options->get('homepage_content2')); ?>
        </div>
        <div class="wrapper">
        	<div class="container clients-logo-front">        
			<?php /* $logores=$wpdb->get_results("select * from client_logo"); 
				  $rowno=count($logores)/5;
				  for($i=0;$i<$rowno;$i++) 
				  {
				  	$perpage=5;
				  	$pageLimit =$perpage * $i;
					$query="select * from client_logo limit $pageLimit,$perpage";
					$logorows=$wpdb->get_results($query); 	
					echo '<div class="row row-shortcode well_small logorow">';
					for($j=0;$j<count($logorows);$j++)
					{
						echo '<div class="span2"><img class="alignnone size-full wp-image-519" alt="logo1" src="'.$upload_dir['baseurl'].'/member_image/'.$logorows[$j]->logo_name.'"></div>';
					}
					echo '</div>';
				}*/
				?>
                <div class="row row-shortcode well_small logorow">
                	<?php 	$query="select * from client_logo";
							$logorows=$wpdb->get_results($query); 	
							for($j=0;$j<count($logorows);$j++)
							{
								echo '<div class="span2"><img class="alignnone size-full wp-image-519" alt="logo1" src="'.$upload_dir['baseurl'].'/member_image/'.$logorows[$j]->logo_name.'"></div>';
							}
					?>
                </div>
            </div>
        </div>
	<?php }

get_footer(); ?>
<script>
jQuery(document).ready(function(){
	jQuery("#portfiliotxt").attr("href","<?php echo get_permalink(627); ?>");
	jQuery("#ftnews").attr("href","<?php echo get_category_link(32);?>");
	jQuery(".more-link").attr("href","<?php echo get_category_link(32);?>");
});	
</script>