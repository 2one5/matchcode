<?php 
/*
Template Name: Marketing Tab 
*/
get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);

        if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail('full', 'featured-image', true );

                if ( !empty($html_title) ) {?>
                    <!--<div class="container">
                        <div class="row">
                            <section class="page-header span12 bigger-headings">
                                <?php wpgrade_display_content( $html_title ); ?>
                            </section>
                        </div>
                    </div>-->
                <?php }?>
            </div>
        <?php }?>
        
			<div class="wrapper entry-header-wrapper dgimg mpzero">
        	<?php include (TEMPLATEPATH.'/blue_bdr.php'); ?>
        </div> 
            <?php $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <div class="wrapper entry-header-wrapper dgimg mpzero">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title exp_title">
							<?php  
								$title=get_the_title();
								$tittle=explode(" ",$title);	
								for($t=0;$t<count($tittle);$t++)
								{
									if($t==0) echo '<span class="exp_t_one_one">'.$tittle[$t].'</span>';
									else{ echo ' <span class="exp_t_two_two">'.$tittle[$t].'</span>';}
								}
							?></h1>
                        <h4 class="exp_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4>
                    </header><!-- .entry-header -->
                </div>
            <?php }?>
            <div class="mkt_img wrapper">
            	<img src="<?php bloginfo('template_url');?>/library/images/Marketing_Code_bg.jpg" />
            	<div class="mkt_title_one">
                	<a id="mkt_a_one" class="mkta_one" href="<?php echo get_permalink(577);?>">&nbsp;<?php //echo get_the_title(577);?></a>
                </div>
                <div class="mkt_title_two">
                	<a id="mkt_a_otwo" class="mkta_two" href="<?php echo get_permalink(579);?>">&nbsp;<?php //echo get_the_title(579);?></a>
                </div>
            </div>
            <div class="wrapper mkt_content">
            	<div class="white_dw_arr">&nbsp;</div>
                <?php the_content() ?>
            </div>
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
            
<?php endwhile; endif;
get_footer(); ?>
<script>
jQuery(document).ready(function(){
	menuhover(626,751);
});	
</script>