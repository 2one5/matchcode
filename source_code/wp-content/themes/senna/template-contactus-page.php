<?php
/*
Template Name: Custom Contact Page
*/
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);?>
        <div class="wrapper page-header-wrapper">
        <span class="ldimgone ldimgtwo ldimgthree ldimgfour"></span>
        	<div class="mapleft_res">
            	<img src="<?php bloginfo('template_url'); ?>/library/images/map1.jpg" /> 
            </div>
            <div class="cntusright_parrt">
            	<div class="cntusright_parrt_wapper">
                	<h4>Matchcode Global Overview</h4>
                    <div class="ctusbrd">&nbsp;</div>
                    
                    
					<?php $resone = $wpdb->get_results("SELECT * FROM country");
					for($ii=0;$ii<count($resone);$ii++)
					{
						$restwo =  $wpdb->get_results("SELECT * FROM `city` WHERE `country_id` = '".$resone[$ii]->id."'");	
						?>	
						<ul id="countryli_<?php echo $resone[$ii]->id;?>" class="displayNone country_ul">
                        <?php
						for($i=0;$i<count($restwo);$i++)
						{?>
							<li><a href="javascript:void(0)" id="countryid_<?php echo $restwo[$i]->id;?>" class="countryid">
								<?php echo $restwo[$i]->name;?></a>
								<div id="city_<?php echo $restwo[$i]->id;?>" class="city displayNone">
									<h1><?php echo $restwo[$i]->name." , ".$restwo[$i]->shortcode;?></h1>
									<div class="conytry_add">
                                    	<span class="cnytuname"><?php echo $resone[$ii]->country;?></span>
                                        <br>
										<span class="cntrwadd"><?php echo $restwo[$i]->address;?></span>
                                    </div>
                                    <div class="add_twobdr">&nbsp;</div>    
									<div class="mblno">
                                    	<?php echo $restwo[$i]->phone;?>
                                    </div>
                                    <div class="cusmail">        
											<a href="mailto:<?php echo $restwo[$i]->email;?>">
											<?php echo $restwo[$i]->email."<br/>";?></a>
									</div>
                              	</div>      
							</li>
						<?php } ?>
                        </ul>
                        <?php }?>
						<?php $resone = $wpdb->get_results("SELECT * FROM country");
                            $restwo =  $wpdb->get_results("SELECT * FROM `city` WHERE `country_id` = '".$resone[1]->id."'");     
                        ?>
                        <div id="add_detail">
                        <div class="city" id="city_1">
                            <h1><?php echo $restwo[2]->name." , ".$restwo[2]->shortcode;?></h1>
                            <div class="conytry_add">
                            	<span class="cnytuname"><?php echo $resone[1]->country;?></span>
                            	<span class="cntrwadd"><?php echo $restwo[2]->address;?></span>
                            </div>
                            <div class="add_twobdr">&nbsp;</div>   
                            <div class="mblno"><?php echo $restwo[2]->phone;?></div>
                            <div class="cusmail">      
                              <a href="mailto:<?php echo $restwo[2]->email;?>"><?php echo $restwo[2]->email."<br/>";?></a>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
            <img src="<?php bloginfo('template_url'); ?>/library/images/orange_pin.png" class="bobblePin" rel="1" id="img_bubble_1" />
            <img src="<?php bloginfo('template_url'); ?>/library/images/orange_pin.png" class="bobblePin" rel="2" id="img_bubble_2" />
            <img src="<?php bloginfo('template_url'); ?>/library/images/orange_pin.png" class="bobblePin" rel="3" id="img_bubble_3" />
            <img src="<?php bloginfo('template_url'); ?>/library/images/orange_pin.png" class="bobblePin" rel="4"  id="img_bubble_4"/>
        </div>
        <?php $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);?>
        <div class="wrapper entry-header-wrapper dgimg">
            <header class="entry-header">
            	<hr>
            	<!-- <h1 class="entry-title exp_title"><span class="exp_t_two"><span><?php the_title(); ?></span></span></h1> -->
            	<!-- <h4 class="exp_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4> -->
                <!-- <div class="entry-title" id="ctssc"> -->
                	<!-- <a class="ctstw" href="<?php echo get_option( 'match_tw_url' ); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> -->
                    <!-- <a class="ctsfb" href="<?php echo get_option( 'match_fb_url' ); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> -->
                    <!-- <a class="ctsin" href="<?php echo get_option( 'match_ld_url' ); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> -->
                    <!-- <a class="ctsyt" href="<?php //echo get_option( 'match_yt_url' ); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> -->
                <!-- </div> -->
            </header><!-- .entry-header -->
        </div>
        <div class="wrapper w100">
			<div class="container">
				<div class="onlytwitter">
                	<?php 	
                    if ( is_active_sidebar( 'twitter' ) ) : 
                        dynamic_sidebar( 'twitter' ); 
                    endif;
                    ?>
                </div>    
        	</div>
        </div>
        <div class="wrapper dgimg" style=" padding-bottom:0;">
        	<div class="container">
            	<div class="careers contactuscnt">
                  <?php the_content() ?>
            	</div>
            </div>  
        </div>
        <div class="wrapper mpzero">
            <div class="slider_bdr">&nbsp;</div>
            <div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
        </div>
        <div class="wrapper mpzero">
            <div id="bottumslider">
                <?php include TEMPLATEPATH . '/content-slider.php';  ?>
            </div>        
        </div>
		<?php
    			//comments_template();
    			endwhile; endif;
get_footer(); ?>
<script>
jQuery(document).ready(function(){
	menuhover(705,754);
});	
</script>
<script>
jQuery(document).ready(function(){
	
	jQuery("#countryli_2").css('display','block');
	jQuery("#img_bubble_2").attr('src','<?php bloginfo('template_url'); ?>/library/images/blue_pin.png');
	jQuery(".bobblePin").live("click",function(){
		var rel=jQuery(this).attr("rel");
		jQuery("#countryli_"+rel).css('display','block');
		jQuery(this).attr('src','<?php bloginfo('template_url'); ?>/library/images/blue_pin.png');
		jQuery("#add_detail").css('display','none');
		for(var i=1;i<5;i++)
		{
			if(i!=rel)
			{
			  jQuery("#countryli_"+i).css('display','none');
			  jQuery("#img_bubble_"+i).attr('src','<?php bloginfo('template_url'); ?>/library/images/orange_pin.png');
			}
		}
	});
	jQuery(".countryid").live("click",function(){ 
		var id=(jQuery(this).attr("id")).split('_');
		jQuery("#add_detail").css('display','block');
		jQuery("#add_detail").html(jQuery("#city_"+id[1]).html());
		
	});
	jQuery(".twicons").html("<img src='<?php bloginfo('template_url');?>/library/images/twitter_icon.png'/>");
});
</script>