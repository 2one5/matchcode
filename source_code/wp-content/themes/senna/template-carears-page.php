<?php 
/*
Template Name: Careers Templat 
*/
get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);

        if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail('full', 'featured-image', true );

                if ( !empty($html_title) ) {?>
                    <!--<div class="container">
                        <div class="row">
                            <section class="page-header span12 bigger-headings">
                                <?php wpgrade_display_content( $html_title ); ?>
                            </section>
                        </div>
                    </div>-->
                <?php }?>
            </div>
        <?php }
        $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <div class="wrapper entry-header-wrapper carr dgimg">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title careers"><?php echo get_the_title();?></h1>
                        <h4 class="carr_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4>
                    </header><!-- .entry-header -->
                </div>
            <?php }?>
			<div class="wrapper entry-header-wrapper blue_tab h46">&nbsp;</div>
            <div class="wrapper newslightbg">
                <div class="container">
                	<div class="careers">
                        <div class="news_post_f_img">
                        <?php 
                        $post_img_id[0]=get_post_meta(get_the_ID(),'kd_featured-image-2_page_id',true);
                        $post_data = get_post($post_img_id[0]); 
                        echo $boxedwrapper_start.' '.$boxedwrapper_end.'<img src="'.$post_data->guid.'" alt="'.$post_data->post_name.'" />';
                        ?>
                        </div>      
                  		<div id="careers_post"><?php the_content() ?></div>
                        <a href="<?php echo get_permalink(616);?>" class="connect_now">CONNECT NOW</a>
            		</div>
                </div>
            </div>
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
<?php endwhile; endif;
get_footer(); ?>
<script>
jQuery(document).ready(function(){
	menuhover(625,755);
});	
</script>
