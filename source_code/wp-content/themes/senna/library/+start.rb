require 'rubygems'
require 'json'

require 'fileutils'

# change to script
Dir.chdir(File.expand_path(File.dirname(__FILE__)))
# run compass compiler
puts 'Compass/Sass now running in the background.'
# puts %x{compass watch -c scss/+config-development.rb}
# puts %x{sass --watch --sourcemap scss:../css}
puts %x{sass --compass --watch --sourcemap scss:css}
