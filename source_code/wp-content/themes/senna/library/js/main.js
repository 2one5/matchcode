/*-------------------------------------------------------------------------
	
	Senna Project

	1.	Helper Functions
	2.	Plugin Initialization
	3.	Shortcode Specific
	4.	Header + Search
	5.	Page Specific
	6.	Cross Browser Fixes

-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/*	1.	Helper Functions
/*-------------------------------------------------------------------------*/

	function is_touch_device() {
	  return !!('ontouchstart' in window) // works on most browsers 
	      || !!('onmsgesturechange' in window); // works on ie10
	};

	//Animate entraces elements on scroll
	jQuery.fn.scroll_animate = function() {
		var self = this;
		this.addClass('s-monitor s-hidden');
		if(this.length) {
			var watcher = scrollMonitor.create( this, -0 );
			watcher.enterViewport(function() {
				jQuery.each(self, function(i, e) {
					setTimeout(function() {
						jQuery(e).removeClass('s-hidden').addClass('s-visible');
					}, 125*i)
				})
			});
		}
    }

	//Media Players Video - Youtube & Vimeo
	jQuery(window).load(function() {

	    var vimeoPlayers = jQuery('.flexslider').find('iframe.vimeo_frame'), player;
	    for (var i = 0, length = vimeoPlayers.length; i < length; i++) {
	        player = $f(vimeoPlayers[i]);
	        player.addEvent('ready', ready);
	    }

	    function addEvent(element, eventName, callback) {
	        if (element.addEventListener) {
	            element.addEventListener(eventName, callback, false)
	        } else {
	            element.attachEvent(eventName, callback, false);
	        }
	    }

	    function ready(player_id) {

	        var froogaloop = $f(player_id);
	        froogaloop.addEvent('play', function(id) {
	            jQuery('.flexslider').flexslider('pause');
	        });
	        froogaloop.addEvent('pause', function(id) {
	            jQuery('.flexslider').flexslider('pause');
	        });
			froogaloop.addEvent('finish', function(id) {
	            jQuery('.flexslider').flexslider('play');
	        });
	    }

	    function create_youtube_player(self){
	        var this_player = new YT.Player(jQuery(self).attr('id'), {
	            videoId: jQuery(self).data('ytid'),
	            playerVars: { 'controls': 1, 'modestbranding': 1, 'showinfo': 0, 'html5': 1 },
	            events: {

	                'onStateChange': function (event) {
	                    if (event.data == YT.PlayerState.PLAYING ) {
	                        // Pause Slider while Playing the Video
	                        jQuery('.flexslider').flexslider("pause");
	                    }
	                    if (event.data == YT.PlayerState.PAUSED ) {
	                        // Play Slider while Video is paused
	                        jQuery('.flexslider').flexslider("play");
	                    }
	                }
	            }
	        });
	    }
		
		//function onYouTubeIframeAPIReady() {
			jQuery('.youtube_frame').each(function(){
				self = this;
				create_youtube_player(self);
				jQuery(".slide-video").fitVids();
			});
		//}

	});


/*-------------------------------------------------------------------------*/
/*	2.	Plugin Initialization
/*-------------------------------------------------------------------------*/

;(function ($) {
$(document).ready(function(){

	//Set textarea from contact page to autoresize
	if($("textarea").length) { $("textarea").autoresize(); }

	//Responsive Videos
	if($(".slide-video").length) { $(".slide-video").fitVids(); }

	// Responsive Portfolio Titles
	// if($('.portfolio_items .title').not('.title-plus').length) { $('.portfolio_items .title').not('.title-plus').fitText() }

	//Smooth Scrolling
	function niceScrollInit() {
		$("html").niceScroll({
	     	zindex: 9999,
	     	cursoropacitymin: 0.3,
	     	cursorwidth: 7,
	     	cursorborder: 0,
	     	mousescrollstep: 40,
	     	scrollspeed: 100
	    });
	}

	var smooth_scroll = $('html').attr('data-smooth-scroll'); 
    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod|Macintosh)/g) ? true : false );
    if( smooth_scroll == 'on' && $(window).width() > 680 && !is_touch_device() && !iOS){ niceScrollInit(); }

    //Magnific Popup for Project Page
    function project_page_popup(e) {
    	e.magnificPopup({
		  	delegate: 'li a',
		  	type: 'image',
		  	image: { titleSrc: '' },
		  	gallery: { enabled:true },
			removalDelay: 300,
		  	mainClass: 'pxg-slide-bottom'
		});
    }

    var project_single_gallery = $('.portfolio_single_gallery ul');
    if(project_single_gallery.length) { project_page_popup(project_single_gallery); } 
    
    //Magnific Popup for any other <a> tag that link to an image
    function blog_posts_popup(e) {
    	e.magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			removalDelay: 300,
		  	mainClass: 'pxg-slide-zoom',
			image: { verticalFit: true }
		});
    }

    var blog_posts_images = $('.post a[href$=".jpg"], .post a[href$=".png"], .page a[href$=".jpg"], .page a[href$=".png"]');
    if(blog_posts_images.length) { blog_posts_popup(blog_posts_images); }

    //MediaPlayerJS plugin for audio and video
    var media_elements = $('audio, video');
    if(media_elements.length) {
    	media_elements.mediaelementplayer({
			videoWidth: '100%',
			videoHeight: '100%',
			audioWidth: '100%',
			features: ['playpause','progress','tracks','volume','fullscreen'],
			videoVolume: 'horizontal',
			enableAutosize: true,
	        success: function(mediaElement, domObject){

	            var slider = $(domObject).parents('.flexslider');
	            if ( slider.length > 0 ) {
	                $(mediaElement).on('playing' , function(){
	                    slider.flexslider('pause');
	                });

	                $(mediaElement).on('pause' , function(){
	                    slider.flexslider('play');
	                });
	            }
	        }
		});
    }

/***************** Frontpage Slider ******************/

    var slider_wrapper = $('.wp_slider');

    //One Slide Case - add flex-active-slide class
	if(slider_wrapper.length) {
		slider_wrapper.each( function(index) { 
			if (($(this).find('.slides').children().length) == 1) {
		        	$(this).find('.slide').addClass('flex-active-slide');
		    }
		});
	}

    //Call FlexSlider
    function frontpage_slider() {
		$('.flexslider').flexslider({
	        animation: "fadecss",
	        controlNav: false,
	        useCSS: false,
	        smoothHeight: false,  
	        animationSpeed: 1000,
	        video: true,
	        pauseOnHover: false,
	        slideshow: true,
	        before: function(slider){
	            // when we change a slide we need to stop the playing video
	            var vimeo_frame = slider.slides.eq(slider.currentSlide).find('iframe.vimeo_frame'),
	                youtube = slider.slides.eq(slider.currentSlide).find('.youtube_frame'),
	                mejs_container = slider.slides.eq(slider.currentSlide).find('.mejs-container');

	            if ( youtube.length !== 0)
	                youtube[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');

	            if ( vimeo_frame.length !== 0)
	                $f(  vimeo_frame.attr('id') ).api('pause');


	            if ( mejs_container.length !== 0) {
	                $(mejs_container).find('video')[0].pause();
	            }
	             $(window).trigger('resize focus');
	             slider.slides.removeClass('s-hidden');   
	        },
	        after: function(slider) {
	        	slider.slides.not(':eq('+slider.currentSlide+')').addClass('s-hidden');
	        },
	        start: function(slider) {
	        	 if ( !slider.slides ) return;
	        	slider.slides.not(':eq('+slider.currentSlide+')').addClass('s-hidden');
	        }
	    });
    }

	if((slider_wrapper.length) && (($('.slider-front-page').find('.slides').children().length) != 1)) { frontpage_slider() }
	
/***************** Other Sliders ******************/ 	

	//Twitter Widget Slider
	var twitter_slider = $(".twitter-slider");
	function twitter_slideshow(e) {
		e.flexslider({
            controlNav: true,
            directionNav: false,
            keyboard: false,
            animation: 'fadecss',
			slideshowSpeed: 6000,
	        animationSpeed: 1000,
	        pauseOnHover: true,
	        pauseOnAction: true,
            slideshow: true
        });
	}

	if(twitter_slider.length) { twitter_slideshow(twitter_slider) }

    //Gallery Post Format Slider
	var gallery_format = $('.gallery');
	function gallery_format_slideshow(e) {	
		//Create the specific markup for flexslider
		e.addClass('slides');
    $('.gallery-item').addClass('slide');
    $('.gallery > br').remove();

    $('.gallery_format_slider').flexslider({
        animation: "fadecss",
        selector: ".gallery > dl",
        useCSS: false,
        controlNav: false,
        directionNav: true,
        prevText: "",     
		nextText: "",  
        keyboard: false,
        slideshow: false,
        smoothHeight: false,
        start: function(slider){
        	setTimeout(function() {
            	slider.resize();
        	}, 800);
        }
    });
	}

	if(gallery_format.length) { gallery_format_slideshow(gallery_format) }
		
/*-------------------------------------------------------------------------*/
/*	3.	Shortcode Specific
/*-------------------------------------------------------------------------*/

	// Circle
	function circle_shortcode(e) {
		e.knob({
		 	readOnly: true,
		 	thickness: 0.10,
		 	bgColor: "#e8e8e8",
		 	skin: "tron",
		 	width: 240,
		 	height: 240, 
		 	cursor: false
		});
	}
	var circle_shc = $('.dial');
	if(circle_shc.length) { circle_shortcode(circle_shc) }
	
	// Progressbar
	function progressbar_shortcode(e) {
		e.each(function() {
			var self = $(this).find('.progressbar-progress');
			self.css({'width': self.data('value')});
		})
	}
	var progressbar_shc = $('.progressbar');
	if(progressbar_shc.length) {
		progressbar_shortcode(progressbar_shc);
		progressbar_shc.scroll_animate(); 
	}
	
/*-------------------------------------------------------------------------*/
/*	4.	Header + Search
/*-------------------------------------------------------------------------*/	
	
	//Set retina size logo
	var retina = window.devicePixelRatio > 1 ? true : false;
	if (retina && $('.site-logo').data('retina_logo')) {
		$('.site-logo').attr('src', $('.site-logo').data('retina_logo'));
    }

    //Append search toggle icon into menu items
    $('.search-toggle').appendTo('.site-navigation.desktop .site-mainmenu').wrap('<li>').css('float', 'left');
    var searchForm = $('.site-navigation.desktop .search-form').clone();
    var responsiveSearch = searchForm.wrap('<li/>').wrap('<div class="search-container"/>');
    responsiveSearch = responsiveSearch.closest('li');
    responsiveSearch
        .find('form').unwrap().end()
        .find('.search-query').removeClass('search-query').end()
        .find('.row-background').remove().end()
        .find('.search-remove').remove().end()
        .find('.field').attr('placeholder','Search').end()
        .appendTo($('.site-navigation#responsive .site-mainmenu'));

    //Search Fancy Input Animation (only on browsers that support CSS3 and not on mobile devices)
    if (!is_touch_device() && Modernizr.csstransitions) {
        $('#header #searchform input.search-query').fancyInput();
    }

	// Collapse or hide search bar
	$('.search-toggle, .search-remove').on('click', function(e) {

		$('.search-form').toggleClass('is-visible');

		if (!$('.site-header-wrapper').hasClass('search-form-is-visible')) {
			$('.search-form input').trigger('focus');
		} else {
			$('.search-form input').trigger('blur');
		}
		$('.site-header-wrapper').toggleClass('search-form-is-visible');
	});

    // Smaller menu
    var smallerMenu = function() {
        var menu = $('#menu-main-menu'),
            menuWidth = menu.outerWidth(),
            widthSum = 0;

        menu.children().each(function() {
            widthSum = widthSum + $(this).width();
        });

        if (menuWidth > 200 && widthSum > menuWidth) {
            var more, moreSubMenu;
            if (!menu.find('#menu-item-more').length) {
                var more = $('<li id="menu-item-more" class="menu-item menu-parent-item menu-item-added menu-item-more"><a href="#">More</a></li>').insertBefore('.menu-item-search'),
                    moreSubMenu = $('<ul class="sub-menu sub-menu-more"></ul>').appendTo(more);
                widthSum = widthSum + more.outerWidth();
            } else {
                more = $('#menu-item-more');
                moreSubMenu = more.children('ul');
            }

            while (widthSum > menuWidth) {
                var toMove = menu.children().not('.menu-item-added').last();
                widthSum = widthSum - toMove.outerWidth();
                toMove.prependTo(moreSubMenu);
            }
        }
    };

    $(window).on('resize', smallerMenu);
    smallerMenu();

	//Close search bar when press ESC key
	document.onkeydown = function(e) {
		e = e || window.event;
		if (e.keyCode == 27) {
			if ($('.site-header-wrapper').hasClass('search-form-is-visible')) {
				// $('.site-header-wrapper').find('.search-icon').toggleClass('icon-search');
				$('.search-form').toggleClass('is-visible');
				$('.search-form input').trigger('blur');
				$('.site-header-wrapper').toggleClass('search-form-is-visible');
			}
		}
	};

	//Add active class to open sub-menus on responsive navigation
	$('#responsive li.menu-parent-item > a').on('click', function(e) {
		e.preventDefault();
		$('#responsive li.menu-parent-item:not(".active")').removeClass('active');
		$(this).parent().toggleClass('active');
	});

	//Add .l-header-small class to menu when scroll down
	if (!is_touch_device()) { //Disable on touch devices
		$(window).scroll(function() {
			var scroll_position = $(document).scrollTop();
			if(scroll_position > 80) {
				$('html.l-header-fixed').addClass('l-header-small');
			} else {
				$('html.l-header-fixed').removeClass('l-header-small');
			}
		});
	}

	//Make header small by default on small screen sizes
	if(Modernizr.mq('screen and (max-width: 400px)')) {
       $('html').addClass('l-header-small');
   }

	//Add swipe support for offscreen menu on touch devices
	if (is_touch_device()) {
	    var hammertime = $('body').hammer();
	    hammertime.on('swiperight', function(e) {
	        if (e.gesture.startEvent.touches[0].pageX < 20) {
	            window.App.openNav();
	        }
	    });

	    hammertime.on('swipeleft', function(e) {
	        window.App.closeNav();
	    });
	}

/*-------------------------------------------------------------------------*/
/*	5.	Page Specific
/*-------------------------------------------------------------------------*/	
	
	//Wrap ampersand(&) symbol into an wrap
	$('h1, h2, h3, h4, h5, h6').each(function(){
		if ( $(this).find( 'a[title]').length > 0 ) {
			$(this).find( 'a[title]').bestAmpersand();
		} else {
			$(this).bestAmpersand();
		}
	});

	//Blog & Portfolio Explore dropdown
	$('.categories-dropdown-toggle').on('click', function(e) {
		$('.categories-dropdown').toggleClass('is-open');
		e.preventDefault();
		return false;
	});

	//Featured Image Parallax
	if (Modernizr.csstransitions && !is_touch_device()) {
		var top_header = $('.featured-image');
		$(window).scroll(function () {
		  var st = $(window).scrollTop();
		  top_header.css({
		  	'-webkit-transform': 'translateY(' +  -st / 3 + 'px)',
		  	'transform': 'translateY(' +  -st / 3 + 'px)'
			});
		});
	}

	//Add .not-ios helper class
	if (!iOS) { $('body').addClass('not-ios'); }
	
/*-------------------------------------------------------------------------*/
/*	6.	Cross Browser Fixes
/*-------------------------------------------------------------------------*/	

});
})(jQuery);