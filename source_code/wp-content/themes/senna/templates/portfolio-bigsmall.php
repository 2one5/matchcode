<?php 
	global $wpGrade_Options;
	get_template_part('templates/components/portfolio-head'); 
?>
<?php echo $wpGrade_Options->get('portfolio_archive_fullwidth') ? '' : '<div class="container portfolio-boxed">' ?>
<div class="wrapper portfolio-grid">
		<?php if (have_posts()) : ?>
			<?php portfolio_archive_bigsmall($wpGrade_Options->get('portfolio_archive_limit') ? round_to_closest_multiple(absint($wpGrade_Options->get('portfolio_archive_limit')),3) : 6,0); ?>
			<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<?php get_template_part( 'no-results', 'index' ); ?>
		<?php endif; ?>
</div>
<?php echo $wpGrade_Options->get('portfolio_archive_fullwidth') ? '' : '</div>' ?>