<div id="blog_news_img_419" class="page-header-wrapper wrapper-small">
    <?php	
    global $wpGrade_Options;
    if ( $wpGrade_Options->get('blog_header_image') ) { ?>
        <div class="featured-image" style="background-image: url('<?php echo $wpGrade_Options->get('blog_header_image') ?>');" ></div>
    <?php } ?>
	<div class="container">
		<div class="row">
			<header class="page-header span12">&nbsp;</header><!-- .page-header -->
		</div>
	</div>
</div>

<?php 
	if ( $wpGrade_Options->get('blog_display_dropdown') ) {
		get_template_part('templates/components/blog-header-dropdown'); 
	} ?>
<div class="wrapper entry-header-wrapper dgimg">
    <header class="entry-header">
    	<hr>
    	<h1 class="entry-title exp_title">
    		<span class="exp_t_two"><?php if(is_category() ) {	printf( __( '%s', wpGrade_txtd, wpGrade_txtd ), '<span>' . single_cat_title( '', false ) . '</span>' );}?></span></h1>
    	<h4 class="exp_top_h">THE LATEST IN THE WORLD OF MATCHCODE</h4>
    </header><!-- .entry-header -->
</div>
<div class="wrapper entry-header-wrapper blue_tab h46">&nbsp;</div>
<div class="wrapper newslightbg">
	<div class="container">
		<div class="row">
			<div class="site-content span10 offset1 archive" role="main">
            <ul id="news_ul_li" class="tempdata">
				<?php if (have_posts()) : ?>

				<?php /* Start the Loop */ ?>
				<?php while (have_posts()) : the_post(); ?>
				<li>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="row">
						<div class="span10 offset1">
							<?php get_template_part( 'templates/post-templates/newssingle-head', get_post_format() ); ?>
							<div class="entry-content">
								<?php the_content();//echo wpgrade_better_excerpt(get_the_content()); ?>
							</div><!-- .entry-content -->
							
						</div>
					</div>
				</article><!-- #post-<?php the_ID(); ?> -->
			    </li>
			<?php endwhile; ?>
            </ul>
			<?php wp_reset_postdata(); ?>

			<?php //wp_grade_pagination();?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>
			</div>
		</div> <!-- End .row -->
	</div>
    <?php $varmaxpost= get_option('posts_per_page '); if($varmaxpost < $wp_query->max_num_pages){?> 
    <div id="loadmorenews"><a class="menusearchicon" href="javascript:void(0);">Load more posts</a></div>
    <?php } ?>
</div>
<input type="hidden" id="loder_show" value="<?php if($wp_query->max_num_pages > $_REQUEST['paged'])echo 'open';else echo 'close';?> " />
<div class="wrapper mpzero">
    <div class="slider_bdr">&nbsp;</div>
    <div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
</div>
<div class="wrapper mpzero">
    <div id="bottumslider">
        <?php include TEMPLATEPATH . '/content-slider.php';  ?>
    </div>        
</div>