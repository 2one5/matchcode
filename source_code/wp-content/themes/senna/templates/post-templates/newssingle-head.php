<?php	
   global $wpGrade_Options;
   ?>
<header class="entry-header">
	<?php if (is_single()) :?>
		<?php if ( $wpGrade_Options->get('blog_single_show_fullwidth_header') ) : ?>
		<hr />
		<h1 class="entry-title single-title" itemprop="name"><?php echo get_the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title single-title" itemprop="name"><?php echo get_the_title(); ?></h1>
		<?php endif; ?>
	<?php else : ?>
	<hr />
	<h2 class="entry-title"><a href="javascript:void(0);" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', wpGrade_txtd ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<?php endif; ?>
	<div class="posted-on"><?php echo get_the_date(); ?>  
		<div class="categories"><?php
			$categories = wp_get_post_tags(get_the_ID());
			//print_r($categories);
			$separator = ' , ';
			$output = '';
			if($categories){ echo '-';
				foreach($categories as $category) {
					$output .= '<span>' .$category->name.'</span>'.$separator;
				}
			echo trim($output, $separator);
			}
		?>
		</div>
	</div>
</header><!-- .entry-header -->
<!-- <div class="news_post_f_img"> -->
<?php $postformate=get_post_format();
if($postformate=='')
{
	if (wpgrade_get_thumbnail( 'blog-big', 'entry-featured-image' )){
		echo '<div class="news_post_f_img">';
		echo '<a href="javascript:void(0);" title="'.esc_attr( sprintf( __( 'Read more about %s', wpGrade_txtd ), the_title_attribute( 'echo=0' ) ) ).'" rel="bookmark">'; wpgrade_get_thumbnail( 'blog-big', 'entry-featured-image' ); echo '</a>'; 
		echo '</div>';
	}
}
// if($postformate=='quote')
// {
// 	$quote = get_post_meta($post->ID, WPGRADE_PREFIX.'quote', true);
// 	echo '<blockquote>'.$quote.'</blockquote>';
// }
// if($postformate=='audio')
// {
// 	wpGrade_audio_selfhosted($post->ID); 
// }
// if($postformate=='video')
// {
// 	$video_embed = get_post_meta($post->ID, WPGRADE_PREFIX.'video_embed', true);
// 	if( !empty( $video_embed ) ) {
// 		echo '<div class="video-wrap">' . stripslashes(htmlspecialchars_decode($video_embed)) . '</div>';
// 	} else { 
// 		wpGrade_video_selfhosted($post->ID); 
// 	} 
// }
// if($postformate=='gallery')
// {
// 	wpGrade_gallery_slideshow($post); 
// }
?>
<!-- </div> -->