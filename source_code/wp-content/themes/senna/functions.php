<?php
/*
 * Require theme functions.
 * Removing this will break the theme!
 */
require_once('library/wpGrade.php');

if( class_exists( 'kdMultipleFeaturedImages' ) ) 
{

        $args = array(
                'id' => 'featured-image-2',
                'post_type' => 'page',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Featured image ',
                    'set'       => 'Set featured image 2',
                    'remove'    => 'Remove featured image 2',
                    'use'       => 'Use as featured image 2',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-3',
                'post_type' => 'page',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Featured image ',
                    'set'       => 'Set featured image 3',
                    'remove'    => 'Remove featured image 3',
                    'use'       => 'Use as featured image 3',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-4',
                'post_type' => 'page',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Featured image ',
                    'set'       => 'Set featured image 4',
                    'remove'    => 'Remove featured image 4',
                    'use'       => 'Use as featured image 4',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-5',
                'post_type' => 'page',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Featured image ',
                    'set'       => 'Set featured image 5',
                    'remove'    => 'Remove featured image 5',
                    'use'       => 'Use as featured image 5',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-6',
                'post_type' => 'page',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Featured image ',
                    'set'       => 'Set featured image 6',
                    'remove'    => 'Remove featured image 6',
                    'use'       => 'Use as featured image 6',
                )
        );

        new kdMultipleFeaturedImages( $args );
}

if( class_exists( 'kdMultipleFeaturedImages' ) ) 
{

        $args = array(
                'id' => 'featured-image-21',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image',
                    'set'       => 'Set featured image 21',
                    'remove'    => 'Remove featured image 21',
                    'use'       => 'Use as featured image 21',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-22',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 22',
                    'remove'    => 'Remove featured image 22',
                    'use'       => 'Use as featured image 22',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-23',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 23',
                    'remove'    => 'Remove featured image 23',
                    'use'       => 'Use as featured image 23',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-24',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 24',
                    'remove'    => 'Remove featured image 24',
                    'use'       => 'Use as featured image 24',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-25',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 25',
                    'remove'    => 'Remove featured image 25',
                    'use'       => 'Use as featured image 25',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-26',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 26',
                    'remove'    => 'Remove featured image 26',
                    'use'       => 'Use as featured image 26',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-27',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 27',
                    'remove'    => 'Remove featured image 27',
                    'use'       => 'Use as featured image 27',
                )
        );

        new kdMultipleFeaturedImages( $args );
		
		$args = array(
                'id' => 'featured-image-28',
                'post_type' => 'post',      // Set this to post or page
                'labels' => array(
                    'name'      => 'Slider image ',
                    'set'       => 'Set featured image 28',
                    'remove'    => 'Remove featured image 28',
                    'use'       => 'Use as featured image 28',
                )
        );

        new kdMultipleFeaturedImages( $args );
}
?>