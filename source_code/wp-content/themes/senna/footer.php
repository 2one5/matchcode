    </div> <!-- end #content -->
            <?php global $wpGrade_Options;
             if(is_front_page()){if ( $wpGrade_Options->get( 'use_site_wide_box' ) ) {?>
                <div class="wrapper call-to-action-wrapper">
                    <div class="container">
                        <div class="row">
                            <?php
                            $wide_content = $wpGrade_Options->get( 'site_wide_section' );
                            if ( !empty( $wide_content ) ) { ?>
                                <div class="ftone">
                                    <?php wpgrade_display_content( $wide_content ); ?>
                                </div>
                            <?php }
                            $CTA_label = $wpGrade_Options->get( 'site_wide_button_label' );
                            $CTA_link = $wpGrade_Options->get( 'site_wide_button_link' );
                            if ( !empty($CTA_label) && !empty($CTA_link) ) { ?>
                                <div class="fttwo">
                                    <a class="btn" href="<?php echo $CTA_link; ?>"><?php echo $CTA_label; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php }} // end of use_site_wide_box ?>
            <footer class="site-footer-wrapper wrapper">
                <div class="container">
                    <?php get_sidebar('footer'); ?>
                </div>
            </footer>
            <footer class="site-info wrapper">
                <div class="container">
                    <div class="row">
                        <div class="copyright">
                            <?php
                            $copyright =  $wpGrade_Options->get( 'copyright_text' );
                            if ( $copyright ) {
                                wpgrade_display_content( $copyright );
                            }?>
                        </div><!-- .site-info -->
						<?php if ($wpGrade_Options->get('do_social_footer_menu')): ?>
						<div class="footer-social-menu">
                            <?php
                                $social_icons = $wpGrade_Options->get('social_icons');
                                $target = '';
                                if ($wpGrade_Options->get('social_icons_target_blank')) {
                                    $target = ' target="_blank"';
                                }
                                if (count($social_icons)): ?>
                                    <ul class="footer-social-links">
                                        <?php foreach ($social_icons as $domain => $value): if ($value): ?>
                                            <li class="footer-social-link">
                                                <a href="<?php echo $value ?>"<?php echo $target ?>>
                                                    <?php switch($domain) {
                                                        case 'youtube':
                                                            ?><i class="shc icon-e-play"></i><?php echo ucwords($domain);
                                                            break;
                                                        default:
                                                            ?><i class="shc icon-e-<?php echo $domain; ?>"></i><?php echo ucwords($domain);
                                                    } ?>
                                                </a>
                                            </li>
                                        <?php endif; endforeach ?>
                                    </ul>
                                <?php endif; ?>
                        </div>
						<?php else: ?>
                        <div class="footer-menu">
                            <?php wpgrade_footer_nav(); ?>
                        </div>
						<?php endif; ?>
                    </div>
                </div>
            </footer>
        </div>
     </div>   
     
 
     <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/Archivo_Narrow_400.font.js"></script>

<script type="text/javascript">
	//Cufon.replace('#menu-header-menu li a', { fontFamily: 'Archivo Narrow', hover: true });
</script>

     
    <?php wp_footer();?>
	<!-- Google Analytics tracking code -->
	<?php if ( $wpGrade_Options->get( 'google_analytics' ) ) {
		echo $wpGrade_Options->get( 'google_analytics' );
	} ?>
</body>
</html>