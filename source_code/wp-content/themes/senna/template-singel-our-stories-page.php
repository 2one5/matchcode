<?php 
/*
Template Name: Singel Our stories 
*/
get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);

        if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail('full', 'featured-image', true );?>
			</div>
        <?php }
        $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <div class="wrapper entry-header-wrapper dgimg">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title exp_title"><?php echo $title=get_the_title();?></h1>
                        <h4 class="exp_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4>
                    </header><!-- .entry-header -->
                </div>
            <?php }?>
			<div class="wrapper">
                <div class="container ourstr">
                	<?php the_content() ?>
                </div>
            </div>
            
            <div class="wrapper newslightbg">
            	<div class="blue_tab_ourst">
            	<div>
                	<p>
						FILTER RESULTS: 
                	</p>
                </div>
                <div id="ctst">
                	<p class="selected_p">
						<a id="ctstr" href="javascript:void(0);">CUSTOMER STORIES</a> 
                	</p>
                    <span id="ctst_o">&nbsp;</span>
                </div>
                <div id="sttt">
                	<p>
						<a id="strtell" href="javascript:void(0);">STORY TELLING</a></p>
                    <span id="sttt_o">&nbsp;</span>    
                </div>
                <div id="mst">
                	<p>
                    	<a id="mactstr" href="javascript:void(0);">MATCHCODE STORIES</a>
                    </p>
                    <span id="mst_o">&nbsp;</span>
				</div>    
            </div>
            	<div id="customerstory" class="ourstr storyone">
                	<?php post_images_patchwork(37,20); ?>
                </div>
                <div id="storytellinh" class="ourstr storytwo">
                	<?php post_images_patchwork(38,20); ?>
                </div>
                <div id="matchcodestory" class="ourstr storythree">
                	<?php post_images_patchwork(39,20); ?>
                </div>
            </div>
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
            
<?php endwhile; endif;
get_footer(); ?>