<?php 
/*
Template Name: Who We Are Page
*/
get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post();
        global $post;
		global $wpdb;
		$upload_dir = wp_upload_dir();
        // The user can choose to hide the wordpress title and put his own with visual editor
        $html_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_html_title', true);

        if ( has_post_thumbnail( $post->ID ) || !empty($html_title) ) { ?>
            <div class="wrapper page-header-wrapper">
                <?php wpgrade_get_thumbnail('full', 'featured-image', true );

                if ( !empty($html_title) ) {?>
                    <div class="container">
                        <div class="row">
                            <section class="page-header span12 bigger-headings">
                                <?php //wpgrade_display_content( $html_title ); ?>
                            </section>
                        </div>
                    </div>
                <?php }?>
            </div>
        <?php }
        $hide_title = get_post_meta(get_the_ID(), WPGRADE_PREFIX.'page_display_title', true);
        if ( $hide_title != "on" ) {  ?>
                <!--<div class="wrapper entry-header-wrapper">
                    <header class="entry-header">
                        <hr>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header>
                </div>--><!-- .entry-header -->
            <?php }?>
			<div class="wrapper entry-header-wrapper carr dgimg">
                <header class="entry-header">
                    <hr>
                    <h1 class="entry-title careers"><?php echo get_the_title();?></h1>
                    <h4 class="carr_top_h"><?php echo $meta_values = get_post_meta( get_the_ID(),'_senna_page_html_title',true ); ?></h4>
                </header><!-- .entry-header -->
            </div>
            <div class="wrapper" style=" padding-bottom:0;">
                <div class="container">
                  	<div class="whoweare">
				  		<?php the_content() ?>
                  	</div>
                </div>  
                  	<div class="memberspt">&nbsp;</div>
                  	<div class="row row-shortcode mlzero">
                        <!--<div class="row-background full-width gbm"></div>-->
                        <div class="gbm_hd">
                            <hr />
                            <h1><?php echo 'Global Management Board';//get_option('boaer_meber_title'); ?></h1>
                        </div>
                        <div class="gbm_arrow">&nbsp;</div>
                        <ul class="gbmlist">
                    	<?php $BoardMDetailQuery="select * from board_member";
						$BoardMDetailRes=$wpdb->get_results($BoardMDetailQuery);
                        for($i=0;$i< count($BoardMDetailRes);$i++)
						{?>
                        <li>
                        	<div class="gbm_img">
                            	<img src="<?php echo $upload_dir['baseurl'].'/member_image/'.$BoardMDetailRes[$i]->member_image;?>" /> 
                            </div>
                            <div class="gbm_textpart"> 
                                <div class="gbm_name textblue"><?php echo $BoardMDetailRes[$i]->member_name;?></div>
                                <div class="gbm_subt"><?php echo $BoardMDetailRes[$i]->member_title;?></div>
                                <div class="gbm_b_spt">&nbsp;</div>
                                <div class="gbm_desc"><?php echo $BoardMDetailRes[$i]->member_detail;?></div>
                        	</div>
                        </li>    	
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="row row-shortcode mlzero ext-space-cover">
                        <!--<div class="row-background full-width gbm"></div>-->
                        <div class="ext_gbm_hd">
                        	<span class="gblimg">&nbsp;</span>
                            <hr />
                            <h1><?php echo 'Extended Global Management Board'; //get_option('ext_board_meber_title'); ?></h1>
                        </div>
                        <ul class="ext_gbmlist">
                    	<?php $BoardMDetailQuery="select * from extended_global_board";
						$BoardMDetailRes=$wpdb->get_results($BoardMDetailQuery);
                        for($i=0;$i< count($BoardMDetailRes);$i++)
						{?>
                        <li>
                        	<h3 class="ext_gbm_name">
                            	<?php echo $BoardMDetailRes[$i]->extended_name;?> 
                            </h3>
                            <h4 class="ext_gbm_desc">
                            	<?php echo $BoardMDetailRes[$i]->extended_desc;?>
                            </h4>
                        </li>    	
                        <?php } ?>
                        </ul>
                    </div> 
                </div>
            	<?php //portfolio_single_gallery(478) ?>
            <div class="wrapper mpzero">
            	<div class="slider_bdr">&nbsp;</div>
				<div class="slider_txt">SELECT CLIENTS<hr class="center"></div>
            </div>
            <div class="wrapper mpzero">
            	<div id="bottumslider">
            		<?php include TEMPLATEPATH . '/content-slider.php';  ?>
           		</div>        
            </div>
			<?php
    			//comments_template();
    			endwhile; endif;
get_footer(); ?>
<script>
jQuery(document).ready(function(){
	menuhover(543,750);
});	
</script>