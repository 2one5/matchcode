<?php session_start();
/*
Plugin Name: Template Data Management
Plugin URI: http://matchcode.com
Description: Tamplate data for the front-end. 
Version: 1.0
Author: Ideavate
Author URI: http://matchcode.com
*/
define('MMURL', WP_PLUGIN_URL."/".dirname( plugin_basename( __FILE__ ) ) );
define('MMPATH', WP_PLUGIN_DIR."/".dirname( plugin_basename( __FILE__ ) ) );
function tempatedata_plugin_menu() 
{
	add_menu_page( 'Template Data', 'Template Data', 'manage_options', 'temp-mgt', 'temp_mgt_options' );
}

function temp_mgt_options() 
{
	global $wpdb;
	$upload_dir = wp_upload_dir();
	wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['submitlogo']))
	{
		update_option("match_tw_url", $_POST['tw'] );
		update_option("match_fb_url", $_POST['fb'] );
		update_option("match_ld_url", $_POST['ld'] );
		update_option("match_yt_url", $_POST['yt'] );
		update_option("client_buzz", $_POST['client_buzz'] );
		update_option("client_buzz_description", $_POST['client_buzz_description'] );
		update_option("client_name", $_POST['client_name'] );
		if(!empty($_FILES['logo']['name']))
		{
			if($_FILES['logo']['error'] < 1)
			{
				$size=(1024*1024)*1;
				if($_FILES['logo']['size'] <= $size)
				{
					$newname=time().$_FILES['logo']['name'];
					if(move_uploaded_file($_FILES["logo"]["tmp_name"],ABSPATH."/wp-content/uploads/member_image/".$newname))
					{
						update_option("client_buzz_signature", $newname );
					}
					else
					{
						echo 'Signature not uploade. Please try again.';
					}
				}
				else
				{
					echo 'Max file is 8MB.';
				}
			}
		}
	}
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div><h2>Add New Data</h2>
        <form method="post" enctype="multipart/form-data">
        	<table cellspacing="0" class="wp-list-table widefat fixed users wp-logo">
            	<tbody data-wp-lists="list:user" id="the-list">
                	<tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Twitter Url</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'match_tw_url' );?>" type="text" name="tw" id="tw"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Facebook Url</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'match_fb_url' );?>" type="text" name="fb" id="fb"/></div>
                            </div>
						</td>
					</tr><tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Linkedin Url</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'match_ld_url' );?>" type="text" name="ld" id="ld"/></div>
                            </div>
						</td>
					</tr>
                    </tr><tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>YouTube Url</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'match_yt_url' );?>" type="text" name="yt" id="yt"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Client Buzz</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'client_buzz' );?>" type="text" name="client_buzz" id="client_buzz"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Client Buzz Description</b></div>
                                <div class="logo-right">
                                	<textarea name="client_buzz_description" id="client_buzz_description"><?php echo get_option( 'client_buzz_description' );?></textarea></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Signature</b></div>
                                <div class="logo-right">
                                	<?php $sname=get_option( 'client_buzz_signature' ); if(!empty($sname))echo '<img src="'.$upload_dir['baseurl'].'/member_image/'.get_option( 'client_buzz_signature' ).'" height="100" width="200" />'; ?><input type="file" name="logo" id="logo" /></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Client Name</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo get_option( 'client_name' );?>" type="text" name="client_name" id="client_name"/></div>
                            </div>
						</td>
					</tr>
                    <tr>
                    	<td>                                                    
                            <div class="admin-cntr">        
                                <div class="logo-left">&nbsp;</div>
                                <div class="logo-right">
                                	<input type="submit" name="submitlogo" id="submitlogo" value="Save" />
                                </div>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
        </form>                            
    </div>
<?php 
}
add_action( 'admin_menu', 'tempatedata_plugin_menu' );?>