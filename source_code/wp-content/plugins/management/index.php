<?php session_start();
/*
Plugin Name: Member Management
Plugin URI: http://matchcode.com
Description: Member detail for the front-end. 
Version: 1.0
Author: Ideavate
Author URI: http://matchcode.com
*/
define('MMURL', WP_PLUGIN_URL."/".dirname( plugin_basename( __FILE__ ) ) );
define('MMPATH', WP_PLUGIN_DIR."/".dirname( plugin_basename( __FILE__ ) ) );
function member_plugin_menu() 
{
	add_menu_page( 'Management Board', 'Management Board', 'manage_options', 'board-mgt', 'board_mgt_options' );
	add_submenu_page( 'board-mgt', '', '', 'manage_options', 'board-member-handle', 'board_member_function');
	add_submenu_page( 'board-mgt', 'Extended', 'Extended', 'manage_options', 'extended-member-handle', 'extended_member_function');
	add_submenu_page( 'board-mgt', '', '', 'manage_options', 'extended-board-member-handle', 'extended_board_member_function');
	add_submenu_page( 'board-mgt', 'Company Contacts', 'Company Contacts', 'manage_options', 'company-contact-list-handle', 'company_contact_information_list');
	add_submenu_page( 'board-mgt', '', '', 'manage_options', 'company-contact-form-handle', 'company_contact_information_form');
	add_submenu_page( 'board-mgt', 'Clent Logo', 'Clent Logo', 'manage_options', 'logo-list-handle', 'logo_list');
	add_submenu_page( 'board-mgt', '', '', 'manage_options', 'client-logo-form-handle', 'client_logo_information_form');
}

function logo_list()
{
	global $wpdb;
	echo '<script src="http://code.jquery.com/jquery-1.9.1.js"></script>';
	wp_enqueue_script('myscript', '/wp-content/plugins/logo/logo.js');
	$upload_dir = wp_upload_dir();
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['action']))
	{
		if($_POST['action']=='delete')
		{
			for($dlu=0;$dlu<count($_POST['logos']);$dlu++)
			{
				/*$removelogo="select from wp_logo where logo_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
				if(count($sqlres)>0)
				{
					unlink(ABSPATH."wp-content/uploads/member_image/".$sqlres['logo_name']);	
				}*/
				$removelogo="delete from client_logo where logo_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
			}
		}
	}
	$perpage=10;
	$page=0;
	$logo_no_query="select count(logo_id) as total from client_logo";
	$logo_no_res=$wpdb->get_results($logo_no_query);
	$total=$logo_no_res[0]->total;
	$totalPages = ceil($total / $perpage);
	
	if(isset($_REQUEST['paged']))
	{
		$page=$_REQUEST['paged'];
	}
	if(isset($_REQUEST['pagedup']))
	{
		$page=($_REQUEST['pagedup']-1);
	}
	if($totalPages >=$page && $page >= 0) 
	{
		
	}
	else 
	{
		$page = $totalPages;
	}
	$pageLimit =$perpage * $page; 
	$logo_query="select * from client_logo limit $pageLimit,$perpage";
	$logo_res=$wpdb->get_results($logo_query);
	$url = admin_url().'admin.php?page=logo-list-handle';
	$editurl= admin_url().'admin.php?page=client-logo-form-handle';
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div>
        <h2>Member List<a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=client-logo-form-handle">Add New</a></h2>
        <form method="post" action="">
        	<div class="tablenav top">
    			<div class="alignleft actions">
					<select name="action">
						<option selected="selected" value="-1">Bulk Actions</option>
						<option value="delete">Delete</option>
                    </select>
                    <input type="submit" value="Apply" class="button action" id="doaction" name="">
				</div>
                <div class="tablenav-pages">
    				<span class="displaying-num"><?php echo count($memberlist); ?> items</span>
					<span class="pagination-links">
        				<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged=0';} ?>" title="Go to the first page" class="first-page <?php if($page==0){echo 'disabled';}?>">«</a>
						<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page-1);} ?>" title="Go to the previous page" class="prev-page <?php if($page==0){echo 'disabled';}?>">‹</a>
						<span class="paging-input">
            				<input type="text" size="1" value="<?php echo ($page+1);?>" name="pagedup" id="pagedup" title="Current page" class="current-page"> of <span class="total-pages"><?php echo $totalPages;?></span>
						</span>
						<a href="<?php $lastpg=($totalPages-1); if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page+1);} ?>" title="Go to the next page" class="next-page <?php if($page==$lastpg){echo 'disabled';}?>">›</a>
						<a href="<?php if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($totalPages-1);} ?>" title="Go to the last page" class="last-page <?php if($page==$lastpg){echo 'disabled';}?>">»</a>
					</span>
				</div>
				<br class="clear">
			</div>
            <table cellspacing="0" class="wp-list-table widefat fixed users">
            	<thead>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Logo Image</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Client Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</thead>
                <tfoot>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Logo Image</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Client Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</tfoot>
                <tbody data-wp-lists="list:user" id="the-list">
                <?php for($show=0;$show<count($logo_res);$show++){?>
                	<tr class="alternate" id="user-52">
                    	<th class="check-column" scope="row">
                        	<label for="cb-select-52" class="screen-reader-text">Select a</label>
                            <input type="checkbox" value="<?php echo $logo_res[$show]->logo_id;?>" class="subscriber" id="user_<?php echo $logo_res[$show]->logo_id;?>" name="logos[]">
                        </th>    
						<td class="role column-role">
							<?php echo $logo_res[$show]->logo_id;?></td>
                        <td class="role column-role">
							<img src="<?php echo $upload_dir['baseurl'].'/member_image/'.$logo_res[$show]->logo_name;?>" height="60" width="100" /></td>    
                        <td class="posts column-posts num" style="text-align:left;">
							<?php echo $logo_res[$show]->client_name;?></td>
                        <td><a href="<?php echo $editurl.'&id='.$logo_res[$show]->logo_id;?>">Edit</a></td>    
                    </tr>
			<?php }?>
                </tbody>
			</table>
    	</form>                            
    </div>
<?php 
}

function client_logo_information_form()
{
	global $wpdb;
	$upload_dir = wp_upload_dir();
	$redurl = admin_url().'admin.php?page=logo-list-handle';
	@ini_set( 'upload_max_size' , '8M' );
	@ini_set( 'post_max_size', '8M');
	@ini_set( 'max_execution_time', '100' );
	wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['submitmember']))
	{
		if($_FILES['logo']['error'] < 1)
		{
			$size=(1024*1024)*1;
			if($_FILES['logo']['size'] <= $size)
			{
				$newname=time().$_FILES['logo']['name'];
				if(move_uploaded_file($_FILES["logo"]["tmp_name"],ABSPATH."/wp-content/uploads/member_image/".$newname))
				{
					$addlogoQuery="insert into client_logo(logo_name,client_name) values('".$newname."','".$_POST['bmhd']."')";
					$logoRes=$wpdb->query($addlogoQuery);
					echo '<script>window.location.href="'.$redurl.'";</script>';
				}
				else
				{
					echo 'Logo not uploade. Please try again.';
				}
			}
			else
			{
				echo 'Max file is 8MB.';
			}
		}
	}
	if(isset($_POST['updatemember']))
	{
		$flag=true;
		if($_FILES['logo']['error'] < 1)
		{
			$size=(1024*1024)*1;
			if($_FILES['logo']['size'] <= $size)
			{
				$newname=time().$_FILES['logo']['name'];
				if(move_uploaded_file($_FILES["logo"]["tmp_name"],ABSPATH."/wp-content/uploads/member_image/".$newname))
				{
					
				}
				else
				{
					$flag=false;
					echo 'Profile Iamge not uploade. Please try again.';
				}
			}
			else
			{
				$flag=false;
				echo 'Max file is 8MB.';
			}
		}
		if($flag)
		{
			if(!empty($_FILES['logo']['name']))
			{
				$addlogoQuery="update client_logo set logo_name='".$newname."', client_name='".$_POST['bmhd']."' where logo_id='".$_REQUEST['id']."' ";
			}
			else
			{
				$addlogoQuery="update client_logo set client_name='".$_POST['bmhd']."' where logo_id='".$_REQUEST['id']."' ";
			}
			$logoRes=$wpdb->query($addlogoQuery);
			if(!empty($_POST['bmhd']))
			{
				update_option("boaer_meber_title", $_POST['bmhd'] );
			}
			echo '<script>window.location.href="'.$redurl.'";</script>';
		}
	}
	if(isset($_REQUEST['id']))
	{
		$MemberQuery="select * from client_logo where logo_id='".$_REQUEST['id']."' ";
		$MemRes=$wpdb->get_results($MemberQuery);
	}
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div><h2>Add New Logo</h2>
        <form method="post" enctype="multipart/form-data">
        	<table cellspacing="0" class="wp-list-table widefat fixed users wp-logo">
            	<tbody data-wp-lists="list:user" id="the-list">
                	<tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Client Name</b></div>
                                <div class="logo-right">
                                	<input type="text" name="bmhd" id="bmhd" value="<?php if(isset($MemRes[0]->client_name))echo $MemRes[0]->client_name;?>" />
                                </div>
							</div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Upload Logo</b></div>
                                <div class="logo-right">
                                	<?php if(isset($MemRes[0]->logo_name))echo '<img src="'.$upload_dir['baseurl'].'/member_image/'.$MemRes[0]->logo_name.'" height="100" width="200" />'; ?><input type="file" name="logo" id="logo" /></div>
							</div>
						</td>
					</tr>														
                    <tr>
                    	<td>           
							<div class="logo-left">&nbsp;</div>
                            <div class="logo-right">
                              	<?php if(isset($_REQUEST['id'])){?>
                                <input type="submit" name="updatemember" id="updatemember" value="Update Logo" />
                                <?php } else {?>
                                <input type="submit" name="submitmember" id="submitmember" value="Add New Logo" />
                                <?php } ?>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
        </form>                            
    </div>
<?php 

}


function board_mgt_options() 
{
	global $wpdb;
	echo '<script src="http://code.jquery.com/jquery-1.9.1.js"></script>';
	wp_enqueue_script('myscript', '/wp-content/plugins/logo/logo.js');
	$upload_dir = wp_upload_dir();
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['action']))
	{
		if($_POST['action']=='delete')
		{
			for($dlu=0;$dlu<count($_POST['logos']);$dlu++)
			{
				/*$removelogo="select from wp_logo where logo_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
				if(count($sqlres)>0)
				{
					unlink(ABSPATH."wp-content/uploads/member_image/".$sqlres['logo_name']);	
				}*/
				$removelogo="delete from board_member where member_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
			}
		}
	}
	$perpage=10;
	$page=0;
	$logo_no_query="select count(member_id) as total from board_member";
	$logo_no_res=$wpdb->get_results($logo_no_query);
	$total=$logo_no_res[0]->total;
	$totalPages = ceil($total / $perpage);
	
	if(isset($_REQUEST['paged']))
	{
		$page=$_REQUEST['paged'];
	}
	if(isset($_REQUEST['pagedup']))
	{
		$page=($_REQUEST['pagedup']-1);
	}
	if($totalPages >=$page && $page >= 0) 
	{
		
	}
	else 
	{
		$page = $totalPages;
	}
	$pageLimit =$perpage * $page; 
	$logo_query="select * from board_member limit $pageLimit,$perpage";
	$logo_res=$wpdb->get_results($logo_query);
	$url = admin_url().'admin.php?page=board-mgt';
	$editurl= admin_url().'admin.php?page=board-member-handle';
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div>
        <h2>Member List<a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=board-member-handle">Add New</a></h2>
        <form method="post" action="">
        	<div class="tablenav top">
    			<div class="alignleft actions">
					<select name="action">
						<option selected="selected" value="-1">Bulk Actions</option>
						<option value="delete">Delete</option>
                    </select>
                    <input type="submit" value="Apply" class="button action" id="doaction" name="">
				</div>
                <div class="tablenav-pages">
    				<span class="displaying-num"><?php echo count($memberlist); ?> items</span>
					<span class="pagination-links">
        				<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged=0';} ?>" title="Go to the first page" class="first-page <?php if($page==0){echo 'disabled';}?>">«</a>
						<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page-1);} ?>" title="Go to the previous page" class="prev-page <?php if($page==0){echo 'disabled';}?>">‹</a>
						<span class="paging-input">
            				<input type="text" size="1" value="<?php echo ($page+1);?>" name="pagedup" id="pagedup" title="Current page" class="current-page"> of <span class="total-pages"><?php echo $totalPages;?></span>
						</span>
						<a href="<?php $lastpg=($totalPages-1); if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page+1);} ?>" title="Go to the next page" class="next-page <?php if($page==$lastpg){echo 'disabled';}?>">›</a>
						<a href="<?php if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($totalPages-1);} ?>" title="Go to the last page" class="last-page <?php if($page==$lastpg){echo 'disabled';}?>">»</a>
					</span>
				</div>
				<br class="clear">
			</div>
            <table cellspacing="0" class="wp-list-table widefat fixed users">
            	<thead>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Profile Image</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Member Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</thead>
                <tfoot>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Profile Image</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Member Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</tfoot>
                <tbody data-wp-lists="list:user" id="the-list">
                <?php for($show=0;$show<count($logo_res);$show++){?>
                	<tr class="alternate" id="user-52">
                    	<th class="check-column" scope="row">
                        	<label for="cb-select-52" class="screen-reader-text">Select a</label>
                            <input type="checkbox" value="<?php echo $logo_res[$show]->member_id;?>" class="subscriber" id="user_<?php echo $logo_res[$show]->member_id;?>" name="logos[]">
                        </th>    
						<td class="role column-role">
							<?php echo $logo_res[$show]->member_id;?></td>
                        <td class="role column-role">
							<img src="<?php echo $upload_dir['baseurl'].'/member_image/'.$logo_res[$show]->member_image;?>" height="100" width="200" /></td>    
                        <td class="posts column-posts num" style="text-align:left;">
							<?php echo $logo_res[$show]->member_name;?></td>
                        <td><a href="<?php echo $editurl.'&id='.$logo_res[$show]->member_id;?>">Edit</a></td>    
                    </tr>
			<?php }?>
                </tbody>
			</table>
    	</form>                            
    </div>
<?php 
}
function board_member_function() 
{
	global $wpdb;
	$upload_dir = wp_upload_dir();
	$redurl = admin_url().'admin.php?page=board-mgt';
	@ini_set( 'upload_max_size' , '8M' );
	@ini_set( 'post_max_size', '8M');
	@ini_set( 'max_execution_time', '100' );
	wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['submitmember']))
	{
		if($_FILES['logo']['error'] < 1)
		{
			$size=(1024*1024)*1;
			if($_FILES['logo']['size'] <= $size)
			{
				$newname=time().$_FILES['logo']['name'];
				if(move_uploaded_file($_FILES["logo"]["tmp_name"],ABSPATH."/wp-content/uploads/member_image/".$newname))
				{
					$addlogoQuery="insert into board_member(member_image,member_name,member_title,member_detail) values('".$newname."','".$_POST['bmem']."','".$_POST['subdetail']."','".$_POST['detail']."')";
					$logoRes=$wpdb->query($addlogoQuery);
					if(!empty($_POST['bmhd']))
					{
						update_option("boaer_meber_title", $_POST['bmhd'] );
					}
					echo '<script>window.location.href="'.$redurl.'";</script>';
				}
				else
				{
					echo 'Logo not uploade. Please try again.';
				}
			}
			else
			{
				echo 'Max file is 8MB.';
			}
		}
	}
	if(isset($_POST['updatemember']))
	{
		$flag=true;
		if($_FILES['logo']['error'] < 1)
		{
			$size=(1024*1024)*1;
			if($_FILES['logo']['size'] <= $size)
			{
				$newname=time().$_FILES['logo']['name'];
				if(move_uploaded_file($_FILES["logo"]["tmp_name"],ABSPATH."/wp-content/uploads/member_image/".$newname))
				{
					
				}
				else
				{
					$flag=false;
					echo 'Profile Iamge not uploade. Please try again.';
				}
			}
			else
			{
				$flag=false;
				echo 'Max file is 8MB.';
			}
		}
		if($flag)
		{
			if(!empty($_FILES['logo']['name']))
			{
				$addlogoQuery="update board_member set member_image='".$newname."', member_name='".$_POST['bmem']."' ,member_title='".$_POST['subdetail']."',member_detail='".$_POST['detail']."' where member_id='".$_REQUEST['id']."' ";
			}
			else
			{
				$addlogoQuery="update board_member set member_name='".$_POST['bmem']."' ,member_title='".$_POST['subdetail']."',member_detail='".$_POST['detail']."' where member_id='".$_REQUEST['id']."' ";
			}
			$logoRes=$wpdb->query($addlogoQuery);
			if(!empty($_POST['bmhd']))
			{
				update_option("boaer_meber_title", $_POST['bmhd'] );
			}
			echo '<script>window.location.href="'.$redurl.'";</script>';
		}
	}
	if(isset($_REQUEST['id']))
	{
		$MemberQuery="select * from board_member where member_id='".$_REQUEST['id']."' ";
		$MemRes=$wpdb->get_results($MemberQuery);
	}
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div><h2>Add New Logo</h2>
        <form method="post" enctype="multipart/form-data">
        	<table cellspacing="0" class="wp-list-table widefat fixed users wp-logo">
            	<tbody data-wp-lists="list:user" id="the-list">
                	<tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Heading</b></div>
                                <div class="logo-right">
                                	<input type="text" name="bmhd" id="bmhd" value="<?php echo get_option( 'boaer_meber_title' ); ?>" />
                                </div>
							</div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Upload Logo</b></div>
                                <div class="logo-right">
                                	<?php if(isset($MemRes[0]->member_image))echo '<img src="'.$upload_dir['baseurl'].'/member_image/'.$MemRes[0]->member_image.'" height="100" width="200" />'; ?><input type="file" name="logo" id="logo" /></div>
							</div>
						</td>
					</tr>														
                    <tr>	                    	
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Member Name</b></div>
                                <div class="logo-right">
                                	<input value="<?php if(isset($MemRes[0]->member_name))echo $MemRes[0]->member_name;?>" type="text" name="bmem" id="bmem"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Sub Detail</b></div>
                                <div class="logo-right">
                                	<textarea name="subdetail" id="subdetail"><?php if(isset($MemRes[0]->member_title))echo $MemRes[0]->member_title;?></textarea></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Detail</b></div>
                                <div class="logo-right">
                                	<textarea name="detail" id="detail"><?php if(isset($MemRes[0]->member_detail))echo $MemRes[0]->member_detail;?></textarea></div>
                            </div>
						</td>
					</tr>                 
                    <tr>
                    	<td>           
							<div class="logo-left">&nbsp;</div>
                            <div class="logo-right">
                              	<?php if(isset($_REQUEST['id'])){?>
                                <input type="submit" name="updatemember" id="updatemember" value="Update Member Detail" />
                                <?php } else {?>
                                <input type="submit" name="submitmember" id="submitmember" value="Add New Member" />
                                <?php } ?>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
        </form>                            
    </div>
<?php 

}

function extended_member_function() 
{
	global $wpdb;
	echo '<script src="http://code.jquery.com/jquery-1.9.1.js"></script>';
	wp_enqueue_script('myscript', '/wp-content/plugins/logo/logo.js');
	$upload_dir = wp_upload_dir();
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['action']))
	{
		if($_POST['action']=='delete')
		{
			for($dlu=0;$dlu<count($_POST['logos']);$dlu++)
			{
				/*$removelogo="select from wp_logo where logo_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
				if(count($sqlres)>0)
				{
					unlink(ABSPATH."wp-content/uploads/member_image/".$sqlres['logo_name']);	
				}*/
				$removelogo="delete from extended_global_board where  extended_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
			}
		}
	}
	$perpage=10;
	$page=0;
	$logo_no_query="select count( extended_id) as total from extended_global_board";
	$logo_no_res=$wpdb->get_results($logo_no_query);
	$total=$logo_no_res[0]->total;
	$totalPages = ceil($total / $perpage);
	
	if(isset($_REQUEST['paged']))
	{
		$page=$_REQUEST['paged'];
	}
	if(isset($_REQUEST['pagedup']))
	{
		$page=($_REQUEST['pagedup']-1);
	}
	if($totalPages >=$page && $page >= 0) 
	{
		
	}
	else 
	{
		$page = $totalPages;
	}
	$pageLimit =$perpage * $page; 
	$logo_query="select * from extended_global_board limit $pageLimit,$perpage";
	$logo_res=$wpdb->get_results($logo_query);
	$url = admin_url().'admin.php?page=board-mgt';
	$editurl= admin_url().'admin.php?page=extended-board-member-handle';
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div>
        <h2>Member List<a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=extended-board-member-handle">Add New</a></h2>
        <form method="post" action="">
        	<div class="tablenav top">
    			<div class="alignleft actions">
					<select name="action">
						<option selected="selected" value="-1">Bulk Actions</option>
						<option value="delete">Delete</option>
                    </select>
                    <input type="submit" value="Apply" class="button action" id="doaction" name="">
				</div>
                <div class="tablenav-pages">
    				<span class="displaying-num"><?php echo count($memberlist); ?> items</span>
					<span class="pagination-links">
        				<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged=0';} ?>" title="Go to the first page" class="first-page <?php if($page==0){echo 'disabled';}?>">«</a>
						<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page-1);} ?>" title="Go to the previous page" class="prev-page <?php if($page==0){echo 'disabled';}?>">‹</a>
						<span class="paging-input">
            				<input type="text" size="1" value="<?php echo ($page+1);?>" name="pagedup" id="pagedup" title="Current page" class="current-page"> of <span class="total-pages"><?php echo $totalPages;?></span>
						</span>
						<a href="<?php $lastpg=($totalPages-1); if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page+1);} ?>" title="Go to the next page" class="next-page <?php if($page==$lastpg){echo 'disabled';}?>">›</a>
						<a href="<?php if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($totalPages-1);} ?>" title="Go to the last page" class="last-page <?php if($page==$lastpg){echo 'disabled';}?>">»</a>
					</span>
				</div>
				<br class="clear">
			</div>
            <table cellspacing="0" class="wp-list-table widefat fixed users">
            	<thead>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Member Name</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Member Description</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</thead>
                <tfoot>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="username" scope="col">				<span>ID</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Member Name</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="name" scope="col">							<span>Member Description</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="email" scope="col">						<span>Edit Detail</span>
                        </th>
                    </tr>
				</tfoot>
                <tbody data-wp-lists="list:user" id="the-list">
                <?php for($show=0;$show<count($logo_res);$show++){?>
                	<tr class="alternate" id="user-52">
                    	<th class="check-column" scope="row">
                        	<label for="cb-select-52" class="screen-reader-text">Select a</label>
                            <input type="checkbox" value="<?php echo $logo_res[$show]-> extended_id;?>" class="subscriber" id="user_<?php echo $logo_res[$show]-> extended_id;?>" name="logos[]">
                        </th>    
						<td class="role column-role">
							<?php echo $logo_res[$show]-> extended_id;?></td>
                        <td class="posts column-posts num" style="text-align:left;">
							<?php echo $logo_res[$show]->extended_name;?></td>
                        <td class="role column-role">
							<?php echo $logo_res[$show]->extended_desc;?></td> 
                        <td><a href="<?php echo $editurl.'&id='.$logo_res[$show]-> extended_id;?>">Edit</a></td>    
                    </tr>
			<?php }?>
                </tbody>
			</table>
    	</form>                            
    </div>
<?php 
}
function extended_board_member_function() 
{
	global $wpdb;
	$upload_dir = wp_upload_dir();
	$redurl = admin_url().'admin.php?page=extended-member-handle';
	wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['submitmember']))
	{
		$addlogoQuery="insert into extended_global_board(extended_name,extended_desc) values('".$_POST['bmem']."','".$_POST['detail']."')";
		$logoRes=$wpdb->query($addlogoQuery);
		if(!empty($_POST['bmhd']))
		{
			update_option("ext_board_meber_title", $_POST['bmhd'] );
		}
		echo '<script>window.location.href="'.$redurl.'";</script>';
	}
	if(isset($_POST['updatemember']))
	{
		$addlogoQuery="update extended_global_board set extended_name='".$_POST['bmem']."', extended_desc='".$_POST['detail']."' where  extended_id='".$_REQUEST['id']."' ";
		$logoRes=$wpdb->query($addlogoQuery);
		if(!empty($_POST['bmhd']))
		{
			update_option("ext_board_meber_title", $_POST['bmhd'] );
		}
		echo '<script>window.location.href="'.$redurl.'";</script>';
	}
	if(isset($_REQUEST['id']))
	{
		$MemberQuery="select * from extended_global_board where  extended_id='".$_REQUEST['id']."' ";
		$MemRes=$wpdb->get_results($MemberQuery);
	}
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div><h2>Add New Logo</h2>
        <form method="post" enctype="multipart/form-data">
        	<table cellspacing="0" class="wp-list-table widefat fixed users wp-logo">
            	<tbody data-wp-lists="list:user" id="the-list">
                	<tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Heading</b></div>
                                <div class="logo-right">
                                	<input type="text" name="bmhd" id="bmhd" value="<?php echo get_option( 'ext_board_meber_title' ); ?>" />
                                </div>
							</div>
						</td>
					</tr>
                   	<tr>	                    	
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Member Name</b></div>
                                <div class="logo-right">
                                	<input value="<?php if(isset($MemRes[0]->extended_name))echo $MemRes[0]->extended_name;?>" type="text" name="bmem" id="bmem"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Detail</b></div>
                                <div class="logo-right">
                                	<textarea name="detail" id="detail"><?php if(isset($MemRes[0]->extended_desc))echo $MemRes[0]->extended_desc;?></textarea></div>
                            </div>
						</td>
					</tr>                 
                    <tr>
                    	<td>           
							<div class="logo-left">&nbsp;</div>
                            <div class="logo-right">
                              	<?php if(isset($_REQUEST['id'])){?>
                                <input type="submit" name="updatemember" id="updatemember" value="Update Member Detail" />
                                <?php } else {?>
                                <input type="submit" name="submitmember" id="submitmember" value="Add New Member" />
                                <?php } ?>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
        </form>                            
    </div>
<?php 

}

function company_contact_information_list() 
{
	
	global $wpdb;
	echo '<script src="http://code.jquery.com/jquery-1.9.1.js"></script>';
	wp_enqueue_script('myscript', '/wp-content/plugins/logo/logo.js');
	
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['action']))
	{
		if($_POST['action']=='delete')
		{
			for($dlu=0;$dlu<count($_POST['city']);$dlu++)
			{
				/*$removelogo="select from wp_logo where logo_id='".$_POST['logos'][$dlu]."'";
				$sqlres=$wpdb->query($removelogo);
				if(count($sqlres)>0)
				{
					unlink(ABSPATH."wp-content/uploads/member_image/".$sqlres['logo_name']);	
				}*/
				$removeCity="delete from city where id='".$_POST['city'][$dlu]."'";
				$sqlres=$wpdb->query($removeCity);
			}
		}
	}
	$perpage=10;
	$page=0;
	$logo_no_query="select count(id) as total from city";
	$logo_no_res=$wpdb->get_results($logo_no_query);
	$total=$logo_no_res[0]->total;
	$totalPages = ceil($total / $perpage);
	
	if(isset($_REQUEST['paged']))
	{
		$page=$_REQUEST['paged'];
	}
	if(isset($_REQUEST['pagedup']))
	{
		$page=($_REQUEST['pagedup']-1);
	}
	if($totalPages >=$page && $page >= 0) 
	{
		
	}
	else 
	{
		$page = $totalPages;
	}
	$pageLimit =$perpage * $page; 
	$cityQuery="select * from city limit $pageLimit,$perpage";
	$cityRes=$wpdb->get_results($cityQuery);
	$url = admin_url().'admin.php?page=company-contact-list-handle';
	$editurl= admin_url().'admin.php?page=company-contact-form-handle';
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div>
        <h2>Contact List<a class="add-new-h2" href="<?php echo admin_url(); ?>admin.php?page=company-contact-form-handle">Add New</a></h2>
        <form method="post" action="">
        	<div class="tablenav top">
    			<div class="alignleft actions">
					<select name="action">
						<option selected="selected" value="-1">Bulk Actions</option>
						<option value="delete">Delete</option>
                    </select>
                    <input type="submit" value="Apply" class="button action" id="doaction" name="">
				</div>
                <div class="tablenav-pages">
    				<span class="displaying-num"><?php echo count($memberlist); ?> items</span>
					<span class="pagination-links">
        				<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged=0';} ?>" title="Go to the first page" class="first-page <?php if($page==0){echo 'disabled';}?>">«</a>
						<a href="<?php if($page==0){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page-1);} ?>" title="Go to the previous page" class="prev-page <?php if($page==0){echo 'disabled';}?>">‹</a>
						<span class="paging-input">
            				<input type="text" size="1" value="<?php echo ($page+1);?>" name="pagedup" id="pagedup" title="Current page" class="current-page"> of <span class="total-pages"><?php echo $totalPages;?></span>
						</span>
						<a href="<?php $lastpg=($totalPages-1); if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($page+1);} ?>" title="Go to the next page" class="next-page <?php if($page==$lastpg){echo 'disabled';}?>">›</a>
						<a href="<?php if($page==$lastpg){echo 'javascript:void(0);';}else{echo $url.'&paged='.($totalPages-1);} ?>" title="Go to the last page" class="last-page <?php if($page==$lastpg){echo 'disabled';}?>">»</a>
					</span>
				</div>
				<br class="clear">
			</div>
            <table cellspacing="0" class="wp-list-table widefat fixed users">
            	<thead>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="id" scope="col"><span>ID</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="city_name" scope="col"><span>City Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="city_s_name" scope="col"><span>City Short Name</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="address" scope="col"><span>Address</span>
						</th>
                        <th style="" class="manage-column column-name sortable desc" id="email" scope="col"><span>Email</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="edit" scope="col"><span>Edit Detail</span>
                        </th>
                    </tr>
				</thead>
                <tfoot>
                    <tr>
                        <th style="" class="manage-column column-cb check-column" id="cb" scope="col">			
                        	<label for="cb-select-all-1" class="screen-reader-text">Select All</label><input type="checkbox" id="cb-select-all-1">
                        </th>
                        <th style="" class="manage-column column-username sortable desc" id="id" scope="col"><span>ID</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="city_name" scope="col"><span>City Name</span>
                        </th>
                        <th style="" class="manage-column column-email sortable desc" id="city_s_name" scope="col"><span>City Short Name</span>
                        </th>
                        <th style="" class="manage-column column-name sortable desc" id="address" scope="col"><span>Address</span>
						</th>
                        <th style="" class="manage-column column-name sortable desc" id="email" scope="col"><span>Email</span>
						</th>
                        <th style="" class="manage-column column-email sortable desc" id="edit" scope="col"><span>Edit Detail</span>
                        </th>
                    </tr>
				</tfoot>
                <tbody data-wp-lists="list:user" id="the-list">
                <?php for($show=0;$show<count($cityRes);$show++){?>
                	<tr class="alternate" id="user-52">
                    	<th class="check-column" scope="row">
                        	<label for="cb-select-52" class="screen-reader-text">Select a</label>
                            <input type="checkbox" value="<?php echo $cityRes[$show]-> id;?>" class="subscriber" id="user_<?php echo $cityRes[$show]-> id;?>" name="city[]">
                        </th>    
						<td class="role column-role">
							<?php echo $cityRes[$show]-> id;?></td>
                        <td class="posts column-posts num" style="text-align:left;">
							<?php echo $cityRes[$show]->name;?></td>
                            <td class="posts column-posts num" style="text-align:left;">
							<?php echo $cityRes[$show]->shortcode;?></td>
                        <td class="role column-role">
							<?php echo $cityRes[$show]->address;?></td>
                            <td class="role column-role">
							<?php echo $cityRes[$show]->email;?></td> 
                        <td><a href="<?php echo $editurl.'&id='.$cityRes[$show]-> id;?>">Edit</a></td>    
                    </tr>
			<?php }?>
                </tbody>
			</table>
    	</form>                            
    </div>
<?php 
}
function company_contact_information_form() 
{
	
	global $wpdb;
	$upload_dir = wp_upload_dir();
	$redurl = admin_url().'admin.php?page=company-contact-list-handle';
	wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
	if ( !current_user_can( 'manage_options' ) )  
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if(isset($_POST['submitCity']))
	{
		$addCityQuery="insert into city(name,shortcode,email,address,details,phone,country_id) values('".$_POST['name']."','".$_POST['shortcode']."','".$_POST['email']."','".$_POST['address']."','".$_POST['details']."','".$_POST['phone']."','".$_POST['country_id']."')";
		$cityRes=$wpdb->query($addCityQuery);
		echo '<script>window.location.href="'.$redurl.'";</script>';
	}
	if(isset($_POST['updateCity']))
	{
		$updateCityQuery="update city set name='".$_POST['name']."', shortcode='".$_POST['shortcode']."',email='".$_POST['email']."', address='".$_POST['address']."',details='".$_POST['details']."', phone='".$_POST['phone']."',country_id='".$_POST['country_id']."' where id='".$_REQUEST['id']."' ";
		$cityRes=$wpdb->query($updateCityQuery);
		echo '<script>window.location.href="'.$redurl.'";</script>';
	}
	if(isset($_REQUEST['id']))
	{
		$cityQuery="select * from city where  id='".$_REQUEST['id']."' ";
		$cityRes=$wpdb->get_results($cityQuery);
		
	}
	$countryQuery="select * from country";
	$countryRes=$wpdb->get_results($countryQuery);
	?>
	<div class="wrap">
		<div class="icon32" id="icon-users"><br></div><h2>
        <?php if(isset($_REQUEST['id'])){?>
         Update New Contact
        <?php } else {?>
        Add New Contact
        <?php } ?></h2>
        <form method="post" enctype="multipart/form-data">
        	<table cellspacing="0" class="wp-list-table widefat fixed users wp-logo">
            	<tbody data-wp-lists="list:user" id="the-list">
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Country</b></div>
                                <div class="logo-right">
                                	<select name="country_id" id="country_id">
                                    <?php
									for($i=0;$i<count($countryRes);$i++)
									{
									?>	  
                                        <option value="<?php echo $countryRes[$i]->id; ?>"<?php if($countryRes[$i]->id==$cityRes[0]->country_id) {?> selected="selected" <?php } ?>><?php echo $countryRes[$i]->country; ?></option>
                                    <?php
									}
									?>
                                    </select>
                                </div>
							</div>
						</td>
					</tr>
                	<tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>City Name</b></div>
                                <div class="logo-right">
                                	<input type="text" name="name" id="name" value="<?php echo $cityRes[0]->name; ?>" />
                                </div>
							</div>
						</td>
					</tr>
                   	<tr>	                    	
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>City Short Name</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo $cityRes[0]->shortcode; ?>" type="text" name="shortcode" id="shortcode"/></div>
                            </div>
						</td>
					</tr>
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Address</b></div>
                                <div class="logo-right">
                                	<input value="<?php echo $cityRes[0]->address; ?>" type="text" name="address" id="address"/>
                                </div>
                            </div>
						</td>
					</tr>  
                    <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Email</b></div>
                                <div class="logo-right">
                                	<input type="text" name="email" id="email" value="<?php echo $cityRes[0]->email; ?>" />
                                </div>
							</div>
						</td>
					</tr>  
                     <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Phone Number</b></div>
                                <div class="logo-right">
                                	<input type="text" name="phone" id="phone" value="<?php echo $cityRes[0]->phone; ?>" />
                                </div>
							</div>
						</td>
					</tr>   
                     <tr class="alternate" id="user-52">
                    	<td class="posts column-posts num">
                        	<div class="admin-cntr">
                            	<div class="logo-left"><b>Details</b></div>
                                <div class="logo-right">
                                	<textarea name="details" id="details"><?php echo $cityRes[0]->details; ?></textarea>
                                </div>
							</div>
						</td>
					</tr>               
                    <tr>
                    	<td>           
							<div class="logo-left">&nbsp;</div>
                            <div class="logo-right">
                              	<?php if(isset($_REQUEST['id'])){?>
                                <input type="submit" name="updateCity" id="updateCity" value="Update Contact Detail" />
                                <?php } else {?>
                                <input type="submit" name="submitCity" id="submitCity" value="Add New Contact" />
                                <?php } ?>
                            </div>
                        </td>
					</tr>
				</tbody>
			</table>
        </form>                            
    </div>
  
<?php 

}

add_action( 'admin_menu', 'member_plugin_menu' );?>